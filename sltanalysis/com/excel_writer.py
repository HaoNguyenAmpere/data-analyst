'''
Created on Jun 16, 2020

@author: hknguyen
'''
import xlsxwriter
from json_excel_converter import Converter
from json_excel_converter.xlsx import Writer

class MyExcelConverter(Converter):
    
    def convert_multisheet(self, data, writer):
        for _sheetname in data:
            writer.add_sheet(_sheetname)
#             print("Convert '{}'".format(_sheetname))
            self.reset()
            self.convert(data[_sheetname], writer)
        writer.close()
        
    def reset(self):
        self.options = None
        super(MyExcelConverter, self).reset()
        
class MyExcelWriter(Writer):
    
    def add_sheet(self, sheetname):
        if self.workbook is None:
            self.workbook = xlsxwriter.Workbook(self.file)
        self.sheet = self.workbook.add_worksheet(sheetname)
        self.reset()
    
    def close(self):
        if self.workbook is not None:
            self.workbook.close()
        