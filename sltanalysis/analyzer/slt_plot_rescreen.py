import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
import datetime as dt
import numpy as np
from plotly.subplots import make_subplots
import sqlite3
import plotly.io as pio
from plotly.offline import plot
import random
import json


def get_lot_not_rescreen(data_rescreen):
    lst = []
    for lot in data_rescreen.FS_Lot.unique():
        x = data_rescreen[data_rescreen.FS_Lot==lot]
        if(x.RS1_Result.sum()==0):
            lst.append(lot)
    return lst
def remove_lot_not_rescreen(data_rescreen) :
    lst = get_lot_not_rescreen(data_rescreen)
    df1=data_rescreen
    for lot in lst:
        df1=df1[df1['FS_Lot']!=lot]
    return df1

def drop_none_rescreen(data_rescreen_sbin):
    df1=data_rescreen_sbin[data_rescreen_sbin.FS_SBIN!='SBINpass']
    df2=data_rescreen_sbin[data_rescreen_sbin.FS_SBIN=='SBINpass']
    df1=df1.drop(df1.loc[df1['RS1_Result']==0].index)
    df1 = df1.append(df2) 
    return df1
def add_lst_html(report,lst_html):
    for i in lst_html:
        report=report+i
    return report
def convert_fig_to_html(fig):
    pio.write_html(fig,'file.html',full_html=False,include_plotlyjs='cdn')
    html=open("file.html", "r")
    html=html.read()
    return html
def convert_testep(i): 
    if '_FS_' in i:
        test_step = 'FS'
    elif '_RS_' in i or '_RSS_' in i or '_RS1_' in i:
        test_step = 'RS1'
    elif '_RS2_' in i:
        test_step = 'RS2'
    elif '_RS3_' in i:
        test_step = 'RS3'
    else: 
        test_step='N/A'
    return test_step

    
def myfunc(sbin,lst_pass):
    
    if(sbin in lst_pass):
        status='pass'
    else: 
        status='fail'

    return status


# def map_SBin_(df):
#     max_fail = df[df['Label']=='fail']['SBin'].value_counts().index[0]
#     df['label_SBin']='other_fail'
#     df.loc[df['Label']=='pass','label_SBin']='pass'
#     df.loc[df['SBin']==max_fail,'label_SBin']= 'max_fail: '+str(max_fail)
#     return df,max_fail


def count_efused(df_lot,step):
    lst_efused = []
    df_lot_step = df_lot[df_lot.TestStep == step]
    d=0
    for index, row in df_lot_step.iterrows():
        if('EFUSED' in row['Serial'] and (row['SBin']==63 or row['SBin']==65)):
            d+=1
            lst_efused.append(row['Serial'])
    return d,lst_efused


def check_serial_duplicate(df,step):
    lst_serial=[]
    df_step=df[df.TestStep==step]
    dic_ = dict(df_step['Serial'].value_counts())
    for serial, value in dic_.items():
        if(value>1):
            lst_serial.append(serial)
    return lst_serial


def compare(data_count,lot,step):

    data_count_ = data_count[(data_count.lot==lot) ]
    data_count_step = data_count_[data_count_.Step==step]

    _setup = data_count_step.part.sum()
    _rescreen = data_count_step['part screen'].sum()

    _timeout =_setup -_rescreen 
    
    return _setup,_rescreen,_timeout


def convert_str_to_float(df,column):
    df[column] = df[column].str.split("%", n = 0, expand = True)
    df[column] = df[column].astype(np.float)
    return df
def f(x):
    x.loc[-1] = pd.Series([])
    return x

def convert_float_to_str(df,column):
    df[column] = df[column].astype(str)+'%'
    return df

def f(x):
    x.loc[-1] = pd.Series([])
    return x

###  --------------------------so snah voi setuplot---------------------------------------

def count_part_setup_lot(df,df_setup_lot,path):
    # writer = pd.ExcelWriter(path,engine='xlsxwriter', options = {'remove_timezone': True})
    lst_day=[]
    lst_lot = []
    lst_handler=[]
    lst_part=[]
    lst_part_df=[]
    lst_step=[]

    for step in ['FS','RS1','RS2']:
        df_setup_lot_step=df_setup_lot[df_setup_lot.TestStep==step]
        df_step=df[df.TestStep==step]

        for lot in df_setup_lot.Number.unique():
            df_setup_lot_= df_setup_lot_step[df_setup_lot_step.Number==lot]
            df_lot = df_step[df_step.LotNumber==lot]
            for handler in df_setup_lot_.Handler.unique():
                df_setup_lot_handler= df_setup_lot_[df_setup_lot_.Handler==handler]
                df_handler =df_lot[df_lot.HandlerID==handler]
                lday=list(df_setup_lot_handler.parse_CreatedOn.value_counts().sort_index(ascending=True).index)
                # print('lday',lday)
                for idx,day in enumerate(df_setup_lot_handler.parse_CreatedOn.value_counts().sort_index(ascending=True).index):
                    df_setup_lot_handler_day = df_setup_lot_handler[df_setup_lot_handler.parse_CreatedOn==day]
                    # print(day,df_handler.parse_StartTime.unique()[0] )
                    if(len(lday)==1):
                        df_day=df_handler[(df_handler.parse_StartTime>=day)]
                    elif(idx<len(lday)-1):
                        df_day=df_handler[(df_handler.parse_StartTime>=day) & (df_handler.parse_StartTime<lday[idx+1])]
                    elif(idx==len(lday)-1) :
                        df_day=df_handler[(df_handler.parse_StartTime>=day)]

                    if(len(df_setup_lot_handler_day.PartCount.values)>0):
                        partcount=df_setup_lot_handler_day.PartCount.values[0]

                        lst_day.append(day)
                        lst_lot.append(lot)
                        lst_handler.append(handler)
                        lst_part.append(partcount)
                        lst_part_df.append(len(df_day))
                        lst_step.append(step)
    dict_ = {'day': lst_day, 'lot': lst_lot, 'handler': lst_handler, 'part':lst_part,'part screen':lst_part_df, 'Step':lst_step} 

    tmp = pd.DataFrame(dict_)

    tmp=tmp.sort_values(by=['day'],ascending=True)
    # tmp['day'] = tmp['day'].dt.tz_localize(None)
    tmp=tmp.sort_values(by=['day'],ascending=True)
    
    # tmp.to_excel(writer, sheet_name='data')
    # for i in tmp.handler.unique():
    #     tmp_handler = tmp[tmp.handler==i]
    #     tmp_handler.to_excel(writer, sheet_name=i)
    # writer.save()
    return tmp
# data_count = count_part_setup_lot(df,df_setup_lot,'/Users/haon/Desktop/plotly_report/count_part_setup_lot_event_.xlsx')


#####-------------------------------------------
### clean: loc cac TH : thuc su setup nhieu lan hay setup 1 lan

# path= '/Users/haon/Desktop/plotly_report/count_part_setup_lot_event_.xlsx'
# data_count = pd.read_excel(path, sheet_name='data')
# data_count=data_count.sort_values(by=['day'],ascending=True)

def filter_data_setup(data_lot,data_handler,data_step):
    
    lst_day=[]
    lst_lot = []
    lst_handler=[]
    lst_part=[]
    lst_part_df=[]
    lst_step=[]
    list_part=[]

    sum_screen={}
    dem_part_setup={}
    day_tmp=[]

    for index, row in data_step.iterrows():
        key = str(row['part'])
        sum_screen[key] = 0
        dem_part_setup[key]=0
    d=0
    for index, row in data_step.iterrows():
        key = str(row['part'])
        n = len(data_step[data_step.part == row['part']])
        # print('lot',data_step.lot,data_step.handler,data_step.Step)
        if(n==2):
            sum_screen[key]+=row['part screen']
            dem_part_setup[key]+=1  
            d+=1
            if(d==1):
                lst_day.append(row['day'])
                lst_lot.append(row['lot'])
                lst_handler.append(row['handler'])
                lst_part.append(row['part'])
                lst_step.append(row['Step'])
                lst_part_df.append(sum_screen[key])
            else:
                # print('lst_part_df: ',lst_part_df)

                if(row['part']>=sum_screen[key]):
                    lst_part_df[-1] = sum_screen[key]
                else:
                    lst_day.append(row['day'])
                    lst_lot.append(row['lot'])
                    lst_handler.append(row['handler'])
                    lst_part.append(row['part'])
                    lst_part_df.append(row['part screen'])
                    lst_step.append(row['Step'])



        elif(n>2):
            d+=1
            sum_screen[key]+=row['part screen']
            dem_part_setup[key]+=1  
            day_tmp.append(row['day'])
            # print(row['part'],sum_screen[key] )

            if(row['part']>=sum_screen[key] and d==n):
                lst_day.append(day_tmp[0])
                lst_lot.append(row['lot'])
                lst_handler.append(row['handler'])
                lst_part.append(row['part'])
                lst_step.append(row['Step'])
                lst_part_df.append(sum_screen[key] )

                day_tmp=[]
                day_tmp.append(row['day'])

                sum_screen[key] = 0
                dem_part_setup[key] = 0

            elif(row['part']<sum_screen[key]):
                # print(row['part'],sum_screen[key] )
                sum_screen[key] -= row['part screen']

                lst_day.append(day_tmp[0])
                lst_lot.append(row['lot'])
                lst_handler.append(row['handler'])
                lst_part.append(row['part'])
                lst_part_df.append(sum_screen[key] )
                lst_step.append(row['Step'])

                day_tmp=[]
                day_tmp.append(row['day'])


                sum_screen[key] = row['part screen']
                dem_part_setup[key] = 1


        else:

            lst_day.append(row['day'])
            lst_lot.append(row['lot'])
            lst_handler.append(row['handler'])
            lst_part.append(row['part'])
            lst_part_df.append(row['part screen'])
            lst_step.append(row['Step'])


    dict_ = {'day': lst_day, 'lot': lst_lot, 'handler': lst_handler, 'part':lst_part,'part screen':lst_part_df,'Step': lst_step} 
    t = pd.DataFrame(dict_) 
    t=t.sort_values(by=['day'],ascending=True)
    ## when part setup =0 => cong gop voi report truoc do
    lst_day=[]
    lst_lot = []
    lst_handler=[]
    lst_part=[]
    lst_part_df=[]
    lst_step=[]
    list_part=[]
    for index, row in t.iterrows():
        if(row['part']==0):#TAXV36.00X.80.30
            # print(row['day'],lst_part_df)
            lst_part_df[-1] += row['part screen']
        else:
            lst_day.append(row['day'])
            lst_lot.append(row['lot'])
            lst_handler.append(row['handler'])
            lst_part.append(row['part'])
            lst_part_df.append(row['part screen'])
            lst_step.append(row['Step'])

    dict_ = {'day': lst_day, 'lot': lst_lot, 'handler': lst_handler, 'part':lst_part,'part screen':lst_part_df,'Step': lst_step} 
    data_clean = pd.DataFrame(dict_) 
    return data_clean


def clean_data_count_event(data_count):

    lst_df =[]
    for lot in data_count.lot.unique():
        data_lot = data_count[data_count.lot==lot]
        for handler in data_lot.handler.unique():
            data_handler = data_lot[data_lot.handler==handler]
            for step in ['FS','RS1','RS2']:
                data_step = data_handler[data_handler.Step==step]
                # print('---------------------',len(data_lot))
                t_df = filter_data_setup(data_lot,data_handler,data_step)

                lst_df.append(t_df)
    x=pd.concat(lst_df)
    x = x[x['part screen']!=0]
    x=x.sort_values(by=['day'],ascending=True)

    # writer = pd.ExcelWriter('clean_count_event.xlsx',engine='xlsxwriter', options = {'remove_timezone': True})
    # x.to_excel(writer, sheet_name='data')
    # for i in x.handler.unique():
    #     x_handler = x[x.handler==i]
    #     x_handler.to_excel(writer, sheet_name=i)
    # writer.save()
    return x

# data_count_clean = clean_data_count_event(data_count)

#### -----------------get rescreen ---------------------------------------------------

def percent_list_num(lst,s):
    result =[]
    for i in lst:
        result.append(round((i/s)*100,2))
    return result
def count_FS(df_lot,lot,data_count_clean):

    df_lot_fs = df_lot[df_lot.TestStep=='FS']

    fs_sbin = list(df_lot_fs.lbl.value_counts().index)
    if('63'in fs_sbin):
        fs_sbin.remove('63')
    fs_sbin.append('no_id')
    fs_count=[]
    un_id=0
    sbin_65_count=0
    i=0
    serial_in_sbin_fs={}
    lst_lot = []
    for sbin in fs_sbin:
        lst_lot.append(lot)
        sbin=str(sbin)

        if(sbin!='no_id'):
            df_lot_fs_sbin=df_lot_fs[df_lot_fs.lbl==sbin]

            if(sbin=='63'):
                un_id+=len(df_lot_fs_sbin)

            elif(sbin=='65' ):
                for index, row in df_lot_fs_sbin.iterrows():
                    seri = row['Serial']
#                     print(seri)
                    if('EFUSED' in row['Serial']):
#                         print(row['Serial'])
                        un_id+=1
                    else: 
                        sbin_65_count+=1
                fs_count.append(sbin_65_count)

            else:
                fs_count.append(len(df_lot_fs_sbin))
        else:
            
            _setup,_rescreen,time_out=compare(data_count_clean,lot,'FS')
            time_out = max(0,time_out)
            un_id=un_id+time_out

            fs_count.append(un_id)
        i+=1
    
    # fs_yeild = (fs_count/sum(fs_count))*100
    fs_yeild = percent_list_num(fs_count,sum(fs_count))
    fs_yeild = np.round(fs_yeild,2)
    program_fs = df_lot_fs.TestCase.unique()[0]

    
    fs = {'Lot':lst_lot,
        'SBIN': fs_sbin,
          'Result': fs_count,
          'Test_program': program_fs,
           'Yield': fs_yeild}

    df_fs = pd.DataFrame(fs)
    return df_fs,fs_sbin,sum(fs_count),fs_yeild[0]

def count_RS(data,curr_rs,pre_rs,fs_sbin,data_count_clean,sum_fs_count,max_fail) :
    lot = data.LotNumber.unique()[0]
    data.lbl=data.lbl.astype(str)
    data_fs=data[data['TestStep']==pre_rs]
    data_rs1=data[data['TestStep']==curr_rs]    
    dic_fs_fail={}
    dic_num_fs_fail={}
    rs1_count={}
    rs1_count_pass = {}
    rs1_count_other_fail={}
    rs1_count_max_fail={}
    ### 
    un_id_count=0
    un_id_pass=0
    un_id_other_fail=0
    un_id_max_fail=0
    
    sbin65_count=0
    sbin65_pass=0
    sbin65_other_fail=0
    sbin65_max_fail=0
    
    curr_not_in_pre=[]
    df_lot_pre = data[data.TestStep == pre_rs]
    df_lot_pre_fail = df_lot_pre[df_lot_pre.Label=='fail'] 
    df_lot_curr= data[data.TestStep == curr_rs]
    curr_in_pre=0
    for part in df_lot_curr.Serial:
        if(part in df_lot_pre_fail.Serial.unique()):
            curr_in_pre+=1
        else:
            curr_not_in_pre.append(part)
            
    for sbin in fs_sbin:
        if(sbin!='pass'):
            sbin=str(sbin)
            fs_fail_sbin = data_fs[data_fs['lbl']==sbin]
            dic_fs_fail[sbin]=list(fs_fail_sbin.Serial.unique())
            dic_num_fs_fail[sbin]=len(fs_fail_sbin.Serial.unique())
            rs1_count[sbin]=0
            rs1_count_pass[sbin]=0
            rs1_count_other_fail[sbin]=0
            rs1_count_max_fail[sbin]=0
            if(sbin!='no_id'):
                if(sbin=='65'):
                    for seri in dic_fs_fail[sbin]:
                        if seri in data_rs1.Serial.unique():
                            if('EFUSED' not in seri):
                                rs1_count[sbin]+=1
                                seri_rs1 = data_rs1[data_rs1.Serial==seri]
                                if(seri_rs1.label_SBin.values[0]=='pass'):
                                    rs1_count_pass[sbin]+=1
                                elif(str(seri_rs1.SBin.values[0]) != str(sbin)):
                                    rs1_count_other_fail[sbin]+=1
                                elif(str(seri_rs1.SBin.values[0]) == str(sbin)):
                                    rs1_count_max_fail[sbin]+=1

                else:
                    for seri in dic_fs_fail[sbin]:
                        if seri in data_rs1.Serial.unique():
                            rs1_count[sbin]+=1
                            seri_rs1 = data_rs1[data_rs1.Serial==seri]
                            if(seri_rs1.label_SBin.values[0]=='pass'):
                                rs1_count_pass[sbin]+=1
                            elif(str(seri_rs1.SBin.values[0])!= str(sbin)):
                                rs1_count_other_fail[sbin]+=1
                            elif(str(seri_rs1.SBin.values[0]) == str(sbin)):
                                rs1_count_max_fail[sbin]+=1
        else:
            rs1_count['pass']=0
            rs1_count_pass['pass']=0
            rs1_count_max_fail[sbin]=0
            rs1_count_other_fail[sbin]=0
            
    _,_,time_out=compare(data_count_clean,lot,curr_rs) 
    _,_,time_out_pre=compare(data_count_clean,lot,pre_rs)   
    efused,lst_efused = count_efused(data,curr_rs)
    un_id=len(curr_not_in_pre)+time_out
#     print('len:',len(curr_not_in_pre))
    rs1_count['no_id']=un_id

    
    for part in curr_not_in_pre:
        tmp=df_lot_curr[df_lot_curr.Serial==part]
        if(len(tmp)>0):
            if(tmp.iloc[0]['label_SBin']=='pass'):
                rs1_count_pass['no_id']+=1
            # elif(tmp.iloc[0]['label_SBin']=='order_fail'):
            #     rs1_count_other_fail['no_id']+=1
            # elif(tmp.iloc[0]['SBin']== max_fail):
            #     rs1_count_max_fail['no_id']+=1
            else:
                rs1_count_other_fail['no_id']+=1

    return  rs1_count,rs1_count_pass,rs1_count_max_fail,rs1_count_other_fail


def table(df_lot,step,rs1_count,rs1_count_pass,rs1_count_max_fail,rs1_count_other_fail,pre_yeild,sum_fs_count,fs_sbin):
    
    df_lot_rs1=df_lot[df_lot.TestStep==step]

    program_rs1 = df_lot_rs1.TestCase.unique()[0]
    

    rs1_recovery=[]
    for i, j in zip(list(rs1_count_pass.values()),list(rs1_count.values())):
        if(j==0):
            rs1_recovery.append(0)
        else:
            rs1_recovery.append(round(((i/j)*100),2))
    # lot_recovery = (list(rs1_count_pass.values())/sum_fs_count)*100
    lot_recovery = percent_list_num(list(rs1_count_pass.values()),sum_fs_count)
    # lot_recovery=np.round(lot_recovery,2)
    
    sum_lot_recovery = sum(lot_recovery)
    sum_lot_recovery+=pre_yeild
    # print('pre_yeild',pre_yeild)
    lot_recovery[0]=round(sum_lot_recovery,2)
    # print(len(fs_sbin),len(rs1_count.values()),len(program_rs1),len(rs1_count_pass.values()),len(rs1_count_max_fail.values()))
    # print(len(rs1_count_other_fail.values()),len(rs1_recovery),len(lot_recovery))
    rs1 = {'SBIN':fs_sbin,
           'Result': rs1_count.values(),
           'Test_program': program_rs1,
           'Pass':rs1_count_pass.values(),
         'Fail_Same_SBin':rs1_count_max_fail.values(),
        'Fail_Other_SBIN':rs1_count_other_fail.values(),
           'Sbin recovery':rs1_recovery,
           'Lot_recovery':lot_recovery
         }

    df_rs1 = pd.DataFrame(rs1)
    return df_rs1,sum_lot_recovery



### write file lot sumary : all data
def sumary_rescreen(df,data_count_clean,path):
    max_fail = df[df['Label']=='fail']['SBin'].value_counts().index[0]
    # writer = pd.ExcelWriter(path)
    list_=[]
    is_first=0
    for idx,lot in enumerate(df.LotNumber.unique()):
        
        L=[]
        df_lot = df[df.LotNumber==lot]
        if(len(df_lot[df_lot.TestStep=='FS'])>0):
            df_fs,fs_sbin,sum_fs_count,yeild_pass_fs=count_FS(df_lot,lot,data_count_clean)
            df_fs['Yield'] = df_fs['Yield'].astype(str) + '%'
            df_fs['SBIN'] = 'SBIN'+df_fs['SBIN'].astype(str)
            data =df_fs
            name_columns = df_fs.columns
            L1=['FS']+(list(name_columns))
            L1=tuple(L1)
            L.append(L1)
        if(len(df_lot[df_lot.TestStep=='RS1'])>0):

            rs1_count,rs1_count_pass,rs1_count_max_fail,rs1_count_other_fail = count_RS(df_lot,'RS1','FS',fs_sbin,data_count_clean,sum_fs_count,max_fail) 

            df_rs1,lot_recovery_rs1 = table(df_lot,'RS1',rs1_count,rs1_count_pass,rs1_count_max_fail,rs1_count_other_fail,yeild_pass_fs,sum_fs_count,fs_sbin)
            df_rs1['Sbin recovery'] = df_rs1['Sbin recovery'].astype(str) + '%'
            df_rs1['Lot_recovery'] = df_rs1['Lot_recovery'].astype(str) + '%'
            df_rs1['SBIN'] = 'SBIN'+df_rs1['SBIN'].astype(str)
            name_columns = df_rs1.columns

            data =pd.merge(data, df_rs1, on='SBIN', how='outer')
            L2=['RS1']+(list(name_columns))
            L2.remove('SBIN')
            L2=tuple(L2)
            L.append(L2)
        if(len(df_lot[df_lot.TestStep=='RS2'])>0):

            rs2_count,rs2_count_pass,rs2_count_max_fail,rs2_count_other_fail=count_RS(df_lot,'RS2','RS1',fs_sbin,data_count_clean,sum_fs_count,max_fail) 
            df_rs2,lot_recovery_rs2 = table(df_lot,'RS2',rs2_count,rs2_count_pass,rs2_count_max_fail,rs2_count_other_fail,lot_recovery_rs1,sum_fs_count,fs_sbin)
            df_rs2['Sbin recovery'] = df_rs2['Sbin recovery'].astype(str) + '%'
            df_rs2['Lot_recovery'] = df_rs2['Lot_recovery'].astype(str) + '%'
            df_rs2['SBIN'] = 'SBIN'+df_rs2['SBIN'].astype(str)
            name_columns = df_rs2.columns
            data =pd.merge(data, df_rs2, on='SBIN', how='outer')
            L3=['RS2']+(list(name_columns))
            L3.remove('SBIN')
            L3=tuple(L3)
            L.append(L3)
        
        cols=[]
        if(len(L)>0):
            for k in L:
                new=k[0]
                for c in k[1:]:
                    cols.append((new,c))
            data.columns = pd.MultiIndex.from_tuples(cols)
            
            if(is_first==0):
                data_=data
                is_first=1
            else:
                data_ =pd.merge(data_,data,how='outer')

    # data_.to_excel(writer)
    # writer.save()
    return data_

# data_rescreen = sumary_rescreen(df,data_count_clean,'Lot_sumary.xlsx')

def sumary_rescreen_by_sbin(data_rescreen, path):
    
    dict_ = { 'FS_Result': 'sum','FS_Yield': 'mean',
             'RS1_Result':'sum','RS1_Pass':'sum','RS1_Fail_Same_SBin':'sum','RS1_Fail_Other_SBIN':'sum','RS1_Sbin recovery':'mean','RS1_Lot_recovery':'mean', 
             'RS2_Result':'sum',  'RS2_Pass':'sum','RS2_Fail_Same_SBin':'sum', 'RS2_Fail_Other_SBIN':'sum', 'RS2_Sbin recovery':'mean','RS2_Lot_recovery':'mean'}

#     x= pd.read_excel('Lot_sumary.xlsx',skiprows=1)
    # x = x.groupby(['SBIN'], as_index=False).apply(f)
    # x = x.replace('nan', np.NaN)

    data_rescreen = data_rescreen.dropna(how='all')
#     data_rescreen = data_rescreen.drop('Unnamed: 0',1)
    data_rescreen['RS2_Test_program'] = data_rescreen['RS2_Test_program'].replace(np.nan, 'nan')
    data_rescreen['RS1_Test_program'] = data_rescreen['RS1_Test_program'].replace(np.nan, 'nan')

    data_rescreen['FS_Result'] = data_rescreen['FS_Result'].astype(np.float).astype("Int32")

    data_rescreen = convert_str_to_float(data_rescreen,'FS_Yield')
    data_rescreen = convert_str_to_float(data_rescreen,'RS1_Lot_recovery')
    data_rescreen = convert_str_to_float(data_rescreen,'RS1_Sbin recovery')
    data_rescreen = convert_str_to_float(data_rescreen,'RS2_Lot_recovery')
    data_rescreen = convert_str_to_float(data_rescreen,'RS2_Sbin recovery') 

    data_sbin=[]

    for sbin in data_rescreen.FS_SBIN.unique():
        x_sbin =data_rescreen[data_rescreen.FS_SBIN==sbin]
        x_sbin = x_sbin.groupby(['FS_Test_program','RS1_Test_program','RS2_Test_program'], as_index=False).agg(dict_)
        x_sbin['FS_SBIN']=sbin
        data_sbin.append(x_sbin)

    data_rescreen = pd.concat(data_sbin)

    data_rescreen = data_rescreen[['FS_SBIN', 'FS_Result', 'FS_Test_program', 'FS_Yield', 'RS1_Result', 'RS1_Test_program',
           'RS1_Pass', 'RS1_Fail_Same_SBin', 'RS1_Fail_Other_SBIN', 'RS1_Sbin recovery',
           'RS1_Lot_recovery', 'RS2_Result', 'RS2_Test_program', 'RS2_Pass',
           'RS2_Fail_Same_SBin', 'RS2_Fail_Other_SBIN', 'RS2_Sbin recovery',
           'RS2_Lot_recovery']]
    
    lst_col_num_percent =['FS_Yield','RS1_Lot_recovery','RS1_Sbin recovery','RS2_Lot_recovery','RS2_Sbin recovery']
    lst_col_num =['FS_Result','RS1_Result','RS2_Result','RS1_Pass','RS1_Fail_Same_SBin','RS1_Fail_Other_SBIN','RS2_Result','RS2_Pass','RS2_Fail_Same_SBin','RS2_Fail_Other_SBIN']

    for column in lst_col_num_percent:
        data_rescreen[column] = data_rescreen[column].apply(lambda x: round(x, 2))
        
    data_rescreen[lst_col_num] = data_rescreen[lst_col_num].astype(np.float).astype("Int32")
    result = data_rescreen.copy()
    result = result.replace('nan', np.NaN)
    result = result.groupby(['FS_SBIN'], as_index=False).apply(f)
    
    data_rescreen = convert_float_to_str(data_rescreen,'FS_Yield')
    data_rescreen = convert_float_to_str(data_rescreen,'RS1_Lot_recovery')
    data_rescreen = convert_float_to_str(data_rescreen,'RS1_Sbin recovery')
    data_rescreen = convert_float_to_str(data_rescreen,'RS2_Lot_recovery')
    data_rescreen = convert_float_to_str(data_rescreen,'RS2_Sbin recovery')
#     data_rescreen[lst_col_num] = data_rescreen[lst_col_num].astype(np.float).astype("Int32")
    data_rescreen[lst_col_num_percent]=data_rescreen[lst_col_num_percent].replace('nan%','')
    data_rescreen = data_rescreen.replace('nan', np.NaN)

    lst_col_name=list(data_rescreen.columns)
    data_rescreen = data_rescreen.groupby(['FS_SBIN'], as_index=False).apply(f)

    L1 = tuple(['FS']+['FS_SBIN', 'FS_Result', 'FS_Test_program', 'FS_Yield'])
    L2 = tuple(['RS1']+['RS1_Result', 'RS1_Test_program', 'RS1_Pass', 'RS1_Fail_Same_SBin', 'RS1_Fail_Other_SBIN', 'RS1_Sbin recovery', 'RS1_Lot_recovery'])
    L3 = tuple(['RS2']+['RS2_Result', 'RS2_Test_program', 'RS2_Pass', 'RS2_Fail_Same_SBin', 'RS2_Fail_Other_SBIN', 'RS2_Sbin recovery', 'RS2_Lot_recovery'])
#     print(lst_col_name[:4])
#     print(lst_col_name[4:11])
#     print(lst_col_name[11:])
    L=[L1,L2,L3]
    cols=[]
    for k in L:
        new=k[0]
        for c in k[1:]:
            cols.append((new,c))
    data_rescreen.columns = pd.MultiIndex.from_tuples(cols)

    data_rescreen.to_excel(path)
    return result
    
# data_rescreen_sbin = sumary_rescreen_by_sbin(data_rescreen, 'Lot_sumary_new_sbin.xlsx')
# data_rescreen_sbin

def sumary_rescreen_by_lot(data_rescreen, path):

    data_rescreen = data_rescreen.groupby(['FS_Lot'], as_index=False).apply(f)
    data_rescreen = data_rescreen.replace('nan', np.NaN)
    data_rescreen['FS_Result'] = data_rescreen['FS_Result'].astype(np.float).astype("Int32")
#     data_rescreen = data_rescreen.reset_index()

    ##----------------------------
    result = data_rescreen.copy()

    result = convert_str_to_float(result,'FS_Yield')
    result = convert_str_to_float(result,'RS1_Lot_recovery')
    result = convert_str_to_float(result,'RS1_Sbin recovery')
    result = convert_str_to_float(result,'RS2_Lot_recovery')
    result = convert_str_to_float(result,'RS2_Sbin recovery') 
    
    ####-------------------------
    lst_col_name=list(data_rescreen.columns)

    L1 = tuple(['FS']+['FS_Lot','FS_SBIN', 'FS_Result', 'FS_Test_program', 'FS_Yield'])
    L2 = tuple(['RS1']+['RS1_Result', 'RS1_Test_program', 'RS1_Pass', 'RS1_Fail_Same_SBin', 'RS1_Fail_Other_SBIN', 'RS1_Sbin recovery', 'RS1_Lot_recovery'])
    L3 = tuple(['RS2']+['RS2_Result', 'RS2_Test_program', 'RS2_Pass', 'RS2_Fail_Same_SBin', 'RS2_Fail_Other_SBIN', 'RS2_Sbin recovery', 'RS2_Lot_recovery'])

    L=[L1,L2,L3]
    cols=[]
    for k in L:
        new=k[0]
        for c in k[1:]:
            cols.append((new,c))
    # print('cols',cols)
    data_rescreen.columns = pd.MultiIndex.from_tuples(cols)
    data_rescreen.to_excel(path)
    return result
# data_rescreen_by_lot = sumary_rescreen_by_lot(data_rescreen, 'Lot_sumary_new_lot.xlsx')
# data_rescreen_by_lot

####------------------------------------DRAWN CHART----------------------------------------------
                    ####=================================================#####

def split_data_sbin(data_rescreen_sbin):
    dict_ = { 'FS_Result': 'sum','FS_Yield': 'mean',
                 'RS1_Result':'sum','RS1_Pass':'sum','RS1_Fail_Same_SBin':'sum','RS1_Fail_Other_SBIN':'sum','RS1_Sbin recovery':'mean','RS1_Lot_recovery':'mean', 
                 'RS2_Result':'sum',  'RS2_Pass':'sum','RS2_Fail_Same_SBin':'sum', 'RS2_Fail_Other_SBIN':'sum', 'RS2_Sbin recovery':'mean','RS2_Lot_recovery':'mean'}

    data_sbin = []
    for sbin in data_rescreen_sbin.FS_SBIN.unique():
        x_sbin =data_rescreen_sbin[data_rescreen_sbin.FS_SBIN==sbin]
        if(len(x_sbin)>0):
            same = x_sbin.loc[(x_sbin['FS_Test_program'] == x_sbin['RS1_Test_program'] )]
            diff = x_sbin.loc[(x_sbin['FS_Test_program'] != x_sbin['RS1_Test_program'] )]
            same_rs2 = same.loc[(same['RS1_Test_program'] == same['RS2_Test_program'] )]
            diff_rs2 = same.loc[(same['RS1_Test_program'] != same['RS2_Test_program'] )]

            diff = pd.concat([diff,diff_rs2])
            same = same_rs2

            diff = diff.groupby(['FS_SBIN'], as_index=False).agg(dict_)
            same = same.groupby(['FS_SBIN'], as_index=False).agg(dict_)
            diff['status_test'] = 'diff'
            same['status_test'] = 'same'
            data_sbin.append(same)
            data_sbin.append(diff)
    data_sbin = pd.concat(data_sbin)
    data_sbin = data_sbin.reset_index()
    return data_sbin

def pie_rescreen_sbin(data_sbin,num_sbin):
    specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"},{"type": "table"}]]
    lst=['FS','After RS1','After RS2']*1
    lst_fig = []
    lst_color =['aqua','pink','blue','brown','purple','coral','darkturquoise']
    num_sbin+=1
    for status in ['same','diff']:
        trace =[]
        step_rs =[]
        step_num=[]
        fig = make_subplots(rows=1,cols=4,specs=specs,subplot_titles=lst)
        x = data_sbin[data_sbin.status_test==status]
        x = x.sort_values(by=['FS_Yield'],ascending=False)
        pre_FS = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]

        x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass']=pre_FS +x.RS1_Pass.sum()
        x.loc[x['FS_SBIN']=='SBINpass','RS2_Pass']=x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass'] +x.RS2_Pass.sum() 
        x['RS1_Fail'] = x.FS_Result-x.RS1_Pass
        x['RS2_Fail'] = x.RS1_Fail-x.RS2_Pass
        check =0
        x_all=x
        
        if len(x.FS_SBIN.unique())>=num_sbin:
            x_order = x.iloc[num_sbin:,:]
            x = x.iloc[:num_sbin,:]
            check=1
        if(len(x)>0):
            i=1
            for step in ['FS','RS1','RS2']:
                
                labels=[]
                values =[]
                colors=[]
                if(step == 'FS'):
                    lot_recovery_step_col = 'FS_Result'
                    
                    for idx,sbin in enumerate (x.FS_SBIN.unique()):
                        if(sbin=='SBINpass'):
                            colors.append('green')
                        else:
                            colors.append(lst_color[idx])
                            
                        labels.append(sbin)
                        value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                        values.append(value)
                else:
                    lot_recovery_step_col = step+'_Fail'
                    pass_recovery =step +'_Pass'
                    result_step =step+'_Result'
                    x[result_step]=x[result_step].replace(np.nan, 0)
                    x[pass_recovery]=x[pass_recovery].replace(np.nan, 0)
                    if(x_all[result_step].sum()!=0):
                        for idx, sbin in enumerate(x.FS_SBIN.unique()):
                    
                            labels.append(sbin)
                            if(sbin=='SBINpass'):
                                colors.append('green')
                                value = x[x.FS_SBIN==sbin][pass_recovery].values[0]
                                values.append(value)
                            else:
                                value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                                values.append(value)
                                colors.append(lst_color[idx])

                if(check==1):
                    
                    labels.append('SBIN_other_fail')
                    colors.append('yellow')
                    values.append(round(x_order[lot_recovery_step_col].sum(),2))

                fig.add_trace(go.Pie(labels=labels, values=values,marker_colors=colors),1,i)
                step_result = step+'_Result'
                result = x_all[step_result].sum()
                step_rs.append(step)
                step_num.append(result)
                i+=1
            title = 'Rescreen Overall Lot Recovery for '+status+' testProgram'
            fig.update_layout(title= title,height =400)
            fig.update_traces(textposition='inside', textfont_size=14)
            fig.add_trace(go.Table(header=dict(values=step_rs),cells=dict(values=step_num)),1,4)
            fig.update_layout(height=600, width=1400)
            lst_fig.append(fig)
        
    return lst_fig


def pie_lot_rescreen(data_rescreen_by_lot,lot,num_sbin):
    specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"},{"type": "table"}]]
    trace=[]
    step_rs =[]
    step_num=[]

    x = data_rescreen_by_lot[data_rescreen_by_lot.FS_Lot==lot]
    print(len(x))
    x_pass = x[x.FS_SBIN=='SBINpass']
    x_not_pass = x[x.FS_SBIN!='SBINpass']
    x_not_pass = x_not_pass.sort_values(by=['FS_Yield'],ascending=False)
    x=x_pass.append(x_not_pass)
    
    lst_color =['aqua','pink','blue','brown','purple','coral','darkturquoise']
    
    for index, row in x.iterrows():
        if(row['FS_Result']<row['RS1_Result']):
            row['FS_Result'] = row['RS1_Result']
            x.loc[index,'FS_Result']=row['RS1_Result']
    
    if('SBINpass' in x.FS_SBIN.unique()):
        pre_FS = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]
        x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass']=pre_FS +x.RS1_Pass.sum()
        x.loc[x['FS_SBIN']=='SBINpass','RS2_Pass']=x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass'] +x.RS2_Pass.sum() 
    x['RS1_Fail'] = x.FS_Result-x.RS1_Pass
    x['RS2_Fail'] = x.RS1_Fail-x.RS2_Pass

    lst = ['FS '+str(x.FS_Test_program.values[0]),'After RS1 '+str(x.RS2_Test_program.values[0]),'After RS2 '+str(x.RS2_Test_program.values[0])]*1
    lst=[]
    for step in ['FS','RS1','RS2']:
        test_case = step+'_Test_program'
        if(x[test_case].values[0]!=np.nan):
            name = step+' '+str(x[test_case].values[0])
        else:
            name = step
        lst.append(name)
    lst=lst*1
    fig = make_subplots(rows=1,cols=4,specs=specs,subplot_titles=lst)
    x_all = x
    check =0
    num_sbin+=1
    if len(x.FS_SBIN.unique())>=num_sbin:
        x_order = x.iloc[num_sbin:,:]
        x = x.iloc[:num_sbin,:]
        # print(len(x),len(x_all))
        check=1
    if(len(x)>0):
        i=1
        for step in ['FS','RS1','RS2']:
            labels=[]
            values =[]
            colors=[]
            if(step == 'FS'):
                lot_recovery_step_col = 'FS_Result'
                for idx,sbin in enumerate (x.FS_SBIN.unique()):
                    if(sbin=='SBINpass'):
                        colors.append('green')
                    else:
                        colors.append(lst_color[idx])
                    labels.append(sbin)
                    value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                    values.append(value)
            else:
                lot_recovery_step_col = step+'_Fail'
                pass_recovery =step +'_Pass'
                result_step =step+'_Result'
                x[result_step]=x[result_step].replace(np.nan, 0)
                x[pass_recovery]=x[pass_recovery].replace(np.nan, 0)
                if(x_all[result_step].sum()!=0):
                    for idx, sbin in enumerate(x.FS_SBIN.unique()):
                        labels.append(sbin)
                        if(sbin=='SBINpass'):
                            colors.append('green')
                            value = x[x.FS_SBIN==sbin][pass_recovery].values[0]
                            values.append(value)
                        else:
                            value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                            values.append(value)
                            colors.append(lst_color[idx])


            if(check==1):
                labels.append('SBIN_other_fail')
                values.append(round(x_order[lot_recovery_step_col].sum(),2))
            fig.add_trace(go.Pie(labels=labels, values=values,marker_colors=colors),1,i)
            step_result = step+'_Result'
            result = x_all[step_result].sum()
            # test_case = step+'_Test_program'
            # step_title = step + x[test_case].values[0]
            step_rs.append(step)
            step_num.append(result)
            i+=1

    title = 'Rescreen by lot '+lot
    fig.update_layout(title= title,height =400)
    fig.update_traces(textposition='inside', textfont_size=14)
    fig.add_trace(go.Table(header=dict(values=step_rs),cells=dict(values=step_num)),1,4)
    fig.update_layout(height=600, width=1400)
#     fig.show() 
    return fig
    




def percent_yield_rescreen(data_x,col_pass,fs_sum,fs_pass):
    tmp={}
    x_pass= data_x[data_x.FS_SBIN=='SBINpass']
    x_not_pass = data_x[data_x.FS_SBIN!='SBINpass']
    x_not_pass = x_not_pass.sort_values(by=['FS_Yield'],ascending=False)
    data_x = pd.concat([x_pass,x_not_pass])

    data_x['tmp']=(data_x[col_pass] /fs_sum)*100
    
    for key,val in zip(data_x['FS_SBIN'],data_x['tmp']):
        tmp[key]=round(val,2)
    
    data_yield ={}
    
    lst_x=list(tmp.keys())
    for idx, (key,value) in enumerate (tmp.items()):
            if(idx>0):
                pre_key=lst_x[idx-1]
                data_yield[key]=tmp[key]+data_yield[pre_key]
                if(data_yield[key]>=100):
                    data_yield[key]=100.00
            else:
                data_yield[key]=value+(fs_pass/fs_sum)*100
    data_yield=pd.DataFrame(data_yield.items(), columns=['SBin', 'Yield'])
    data_yield.Yield = round(data_yield.Yield,2)
    data_yield['Text']= data_yield.Yield.astype(str)+'%'

    data_yield_pass = data_yield[data_yield.SBin=='SBINpass']
    data_yield_ = data_yield[data_yield.SBin!='SBINpass']
    data_yield = data_yield_pass.append(data_yield_)

    return data_yield

def percent_yield_rescreen2(data,col_pass,fs_sum,fs_pass):
    x_pass= data[data.FS_SBIN=='SBINpass']
    x_not_pass = data[data.FS_SBIN!='SBINpass']
    x_not_pass = x_not_pass.sort_values(by=['FS_Yield'],ascending=False)
    data = pd.concat([x_pass,x_not_pass])

    tmp={}
    data['RS2_Pass_tmp'] =data['RS1_Pass']+data['RS2_Pass']
    data['tmp']=(data['RS2_Pass_tmp']/fs_sum)*100
    
    for key,val in zip(data['FS_SBIN'],data['tmp']):
        tmp[key]=round(val,2)
    
    data_yield ={}
    
    lst_x=list(tmp.keys())
    for idx, (key,value) in enumerate (tmp.items()):
            if(idx>0):
                pre_key=lst_x[idx-1]
                data_yield[key]=tmp[key]+data_yield[pre_key]
                if(data_yield[key]>=100):
                    data_yield[key]=100.00
            else:
                data_yield[key]=value+(fs_pass/fs_sum)*100
    data_yield=pd.DataFrame(data_yield.items(), columns=['SBin', 'Yield'])
    data_yield.Yield = round(data_yield.Yield,2)
    data_yield['Text']= data_yield.Yield.astype(str)+'%'

    data_yield_pass = data_yield[data_yield.SBin=='SBINpass']
    data_yield_ = data_yield[data_yield.SBin!='SBINpass']
    data_yield = data_yield_pass.append(data_yield_)

    return data_yield


def percent_yield_rescreenfs(data,col_pass,fs_sum,fs_pass):
    tmp={}
    x_pass= data[data.FS_SBIN=='SBINpass']
    x_not_pass = data[data.FS_SBIN!='SBINpass']
    x_not_pass = x_not_pass.sort_values(by=['FS_Yield'],ascending=False)
    data = pd.concat([x_pass,x_not_pass])

    data['tmp']=(data[col_pass] /fs_sum)*100
    for key,val in zip(data['FS_SBIN'],data['tmp']):
        tmp[key]=round(val,2)
    
    data_yield ={}
    
    lst_x=list(tmp.keys())
    for idx, (key,value) in enumerate (tmp.items()):
            if(idx>0):
                pre_key=lst_x[idx-1]
                data_yield[key]=tmp[key]+data_yield[pre_key]
                if(data_yield[key]>=100):
                    data_yield[key]=100.00
            else:
                data_yield[key]=value#+(179/329)*100
    data_yield=pd.DataFrame(data_yield.items(), columns=['SBin', 'Yield'])
    data_yield.Yield = round(data_yield.Yield,2)
    data_yield['Text']= data_yield.Yield.astype(str)+'%'

    data_yield_pass = data_yield[data_yield.SBin=='SBINpass']
    data_yield_ = data_yield[data_yield.SBin!='SBINpass']
    data_yield = data_yield_pass.append(data_yield_)

    return data_yield

def bar_chart_rescreen_lot(data_rescreen_by_lot,lot):
    
    x = data_rescreen_by_lot[data_rescreen_by_lot.FS_Lot==lot]
    for index, row in x.iterrows():
        if(row['FS_Result']<row['RS1_Result']):
            row['FS_Result'] = row['RS1_Result']
            x.loc[index,'FS_Result']=row['RS1_Result']

    x= x.sort_values(by=['FS_Yield'],ascending=False)

    x['RS1_Pass'] = x['RS1_Pass'].replace(np.nan, 0)
    x['RS2_Pass'] = x['RS2_Pass'].replace(np.nan, 0)
    x['text_lot_FS'] = x['FS_Result'].astype(str)

    x['fail_RS1'] = x['FS_Result'] - x['RS1_Pass']
    x['fail_RS2'] = x['FS_Result'] - x['RS1_Pass'] - x['RS2_Pass']

    x['fail_RS1_bar'] = x['fail_RS1']-x['fail_RS2']
    x['FS_Yield_bar'] = x['FS_Result'] - x['RS1_Pass'] - x['RS2_Pass']

    data_x = x[x.FS_SBIN!='SBINpass']
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    if('SBINpass' in x.FS_SBIN.unique()):
        fs_pass = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]
        trace = go.Bar(name='FS pass', x=['SBINpass'], y=[fs_pass],text = [fs_pass],textposition="outside",marker_color='green')
        fig.add_trace(trace,secondary_y=False)
    else:
        fs_pass = 0
    fs_sum = x.FS_Result.sum()
    trace = go.Bar(name='RS1 pass', x=data_x.FS_SBIN, y=data_x['RS1_Pass'],text = data_x['RS1_Pass'],textposition="outside",marker_color='pink')
    fig.add_trace(trace,secondary_y=False)

    trace = go.Bar(name='RS2 pass', x=data_x.FS_SBIN, y=data_x['RS2_Pass'],text = data_x['RS2_Pass'] ,textposition="outside",marker_color='yellow')
    fig.add_trace(trace,secondary_y=False)

    trace = go.Bar(name='FS fail', x=data_x.FS_SBIN, y=data_x['FS_Yield_bar'],text = data_x['text_lot_FS'],textposition="outside",marker_color='darkturquoise')
    fig.add_trace(trace,secondary_y=False)

    data_x =x 
    
    data_yield = percent_yield_rescreenfs(data_x,'FS_Result',fs_sum,fs_pass)
    trace= go.Scatter(name='yield FS',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
    fig.add_trace(trace,secondary_y=True)
    if(x['RS1_Sbin recovery'].sum()!=0):
        data_yield = percent_yield_rescreen(data_x,'RS1_Pass',fs_sum,fs_pass)
        trace= go.Scatter(name='yield RS1 recovery',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
        fig.add_trace(trace,secondary_y=True)
    if(x['RS2_Sbin recovery'].sum()!=0):
        data_yield = percent_yield_rescreen2(data_x,'RS2_Pass',fs_sum,fs_pass)
        trace= go.Scatter(name='yield RS2 recovery',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
        fig.add_trace(trace,secondary_y=True)


    max_range = data_x['FS_Result'].max() +20
    min_line = max(data_yield.Yield.min()-5,0)
    fig.update_yaxes(range=[0, max_range],title_text="count",secondary_y=False)
    fig.update_yaxes(range=[min_line, 103],title_text="percent",secondary_y=True)
    fig.layout.yaxis2.showgrid=False

    fig.update_layout(barmode='stack',title ='Lot_recovery '+lot,height =700,width =1200)
    return fig

def bar_chart_rescreen_sbin(data_sbin,status):
    x= data_sbin[data_sbin.status_test==status]

    x= x.sort_values(by=['FS_Yield'],ascending=False)
    x['RS1_Pass'] = x['RS1_Pass'].replace(np.nan, 0)
    x['RS2_Pass'] = x['RS2_Pass'].replace(np.nan, 0)

    x['text_lot_FS'] = x['FS_Result'].astype(str)

    x['fail_RS1'] = x['FS_Result'] - x['RS1_Pass']
    x['fail_RS2'] = x['FS_Result'] - x['RS1_Pass'] - x['RS2_Pass']

    x['fail_RS1_bar'] = x['fail_RS1']-x['fail_RS2']
    x['FS_Yield_bar'] = x['FS_Result'] - x['RS1_Pass'] - x['RS2_Pass']

    data_x = x[x.FS_SBIN!='SBINpass']
    fig = make_subplots(specs=[[{"secondary_y": True}]])


    fs_pass = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]
    trace = go.Bar(name='FS pass', x=['SBINpass'], y=[fs_pass],text = [fs_pass],textposition="outside",marker_color='green')
    fig.add_trace(trace,secondary_y=False)

    trace = go.Bar(name='RS1 pass', x=data_x.FS_SBIN, y=data_x['RS1_Pass'],text = data_x['RS1_Pass'],textposition="outside",marker_color='pink')
    fig.add_trace(trace,secondary_y=False)

    trace = go.Bar(name='RS2 pass', x=data_x.FS_SBIN, y=data_x['RS2_Pass'],text = data_x['RS2_Pass'] ,textposition="outside",marker_color='yellow')
    fig.add_trace(trace,secondary_y=False)

    trace = go.Bar(name='FS fail', x=data_x.FS_SBIN, y=data_x['FS_Yield_bar'],text = data_x['text_lot_FS'],textposition="outside",marker_color='darkturquoise')
    fig.add_trace(trace,secondary_y=False)

    data_x =x 
    fs_sum = x.FS_Result.sum()
    data_yield = percent_yield_rescreenfs(data_x,'FS_Result',fs_sum,fs_pass)
    trace= go.Scatter(name='yield FS',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
    fig.add_trace(trace,secondary_y=True)
    if(x['RS1_Sbin recovery'].sum()!=0):
        data_yield = percent_yield_rescreen(data_x,'RS1_Pass',fs_sum,fs_pass)
        trace= go.Scatter(name='yield RS1 recovery',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
        fig.add_trace(trace,secondary_y=True)
    if(x['RS2_Sbin recovery'].sum()!=0):
        data_yield = percent_yield_rescreen2(data_x,'RS2_Pass',fs_sum,fs_pass)
        trace= go.Scatter(name='yield RS2 recovery',x=data_yield.SBin,y=data_yield.Yield,text=data_yield.Text,mode='lines+markers+text')
        fig.add_trace(trace,secondary_y=True)


    max_range = data_x['FS_Result'].max() +10
    min_line = max(data_yield.Yield.min()-5,0)
    # print(max_range,min_line)
    fig.update_yaxes(range=[0, max_range],title_text="count",secondary_y=False)
    fig.update_yaxes(range=[min_line, 103],title_text="percent",secondary_y=True)
    fig.layout.yaxis2.showgrid=False
    
    fig.update_layout(barmode='stack',title ='SBin_recovery ',height =700,width =1200)
    return fig




def pie_lot_rescreen_all_sbin(data_rescreen_by_lot,lot):
    trace=[]
    step_rs =[]
    step_num=[]
    lst_color =['aqua','pink','bisque','blue','brown','purple','coral','darkturquoise',
               'burlywood','cadetblue','red','violet','navy','yellow','chocolate','cornsilk',
               'crimson','cyan','darkcyan','darkgoldenrod','aquamarine','gainsboro','orange']
    x = data_rescreen_by_lot[data_rescreen_by_lot.FS_Lot==lot]
    for index, row in x.iterrows():
        if(row['FS_Result']<row['RS1_Result']):
            row['FS_Result'] = row['RS1_Result']
            x.loc[index,'FS_Result']=row['RS1_Result']
    x = x.sort_values(by=['FS_Yield'],ascending=False)
    if('SBINpass' in x.FS_SBIN.unique()):
        pre_FS = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]
        x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass']=pre_FS +x.RS1_Pass.sum()
        x.loc[x['FS_SBIN']=='SBINpass','RS2_Pass']=x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass'] +x.RS2_Pass.sum() 
    x['RS1_Fail'] = x.FS_Result-x.RS1_Pass
    x['RS2_Fail'] = x.RS1_Fail-x.RS2_Pass
    
    # specs = [[{'type':'domain'}] * 3] * 1
    # specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"}],
    #        [{"type": "table"},{"type": "table"},{"type": "table"}]]
    specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"},{"type": "table"}]]

#     lst=['FS','After RS1','After RS2']*1
    lst=[]
    for step in ['FS','RS1','RS2']:
        test_case = step+'_Test_program'
        if(x[test_case].values[0]!=np.nan):
            name = step+' '+str(x[test_case].values[0])
        else:
            name = step
        lst.append(name)
    lst=lst*1
    fig = make_subplots(rows=1,cols=4,specs=specs,subplot_titles=lst)

    if(len(x)>0):
        i=1
        for step in ['FS','RS1','RS2']:
            labels=[]
            values =[]
            colors=[]
            if(step == 'FS'):
                lot_recovery_step_col = 'FS_Result'
                for idx,sbin in enumerate (x.FS_SBIN.unique()):
                    if(sbin=='SBINpass'):
                        colors.append('green')
                    else:
                        colors.append(lst_color[idx])
                    labels.append(sbin)
                    value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                    values.append(value)
            else:
                lot_recovery_step_col = step+'_Fail'
                pass_recovery =step +'_Pass'
                result_step =step+'_Result'
                x[result_step]=x[result_step].replace(np.nan, 0)
                x[pass_recovery]=x[pass_recovery].replace(np.nan, 0)
                if(x[result_step].sum()!=0):
                    for idx, sbin in enumerate(x.FS_SBIN.unique()):
                        labels.append(sbin)
                        if(sbin=='SBINpass'):
                            colors.append('green')
                            value = x[x.FS_SBIN==sbin][pass_recovery].values[0]
                            values.append(value)
                        else:
                            value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                            values.append(value)
                            colors.append(lst_color[idx])

            fig.add_trace(go.Pie(labels=labels, values=values,marker_colors=colors,text = values),1,i)
            step_result = step+'_Result'
            result = x[step_result].sum()
            step_rs.append(step)
            step_num.append(result)
            trace.append(go.Table(header=dict(values=['name', 'total']),cells=dict(values=[[step], [result]])))
            i+=1
    title = 'Rescreen by lot '+lot
    fig.update_layout(title= title,height =400)
    fig.update_traces(textposition='inside', textfont_size=14)
    fig.add_trace(go.Table(header=dict(values=step_rs),cells=dict(values=step_num)),1,4)
    # for idx in range(len(trace)):
    #     fig.add_trace(trace[idx],2,idx+1)
    fig.update_layout(height=600, width=1400)
    return fig

def pie_rescreen_sbin_all_sbin(data_sbin):
    # # specs = [[{'type':'domain'}] * 3] * 1
    # specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"}],
    #        [{"type": "table"},{"type": "table"},{"type": "table"}]]
    specs=[[{"type": "pie"},{"type": "pie"},{"type": "pie"},{"type": "table"}]]
    lst=['FS','After RS1','After RS2']*1
    # fig = make_subplots(rows=2,cols=3,specs=specs,subplot_titles=lst)
    lst_fig = []
    lst_color =lst_color =['aqua','pink','bisque','blue','brown','purple','coral','darkturquoise',
               'burlywood','cadetblue','red','violet','navy','yellow','chocolate','cornsilk',
               'crimson','cyan','darkcyan','darkgoldenrod','aquamarine','gainsboro','orange']
    for status in ['same','diff']:
        trace =[]
        step_rs =[]
        step_num=[]
        fig = make_subplots(rows=1,cols=4,specs=specs,subplot_titles=lst)
        x = data_sbin[data_sbin.status_test==status]
        x = x.sort_values(by=['FS_Yield'],ascending=False)
        pre_FS = x.loc[x['FS_SBIN']=='SBINpass','FS_Result'].values[0]
        x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass']=pre_FS +x.RS1_Pass.sum()
        x.loc[x['FS_SBIN']=='SBINpass','RS2_Pass']=x.loc[x['FS_SBIN']=='SBINpass','RS1_Pass'] +x.RS2_Pass.sum() 
        x['RS1_Fail'] = x.FS_Result-x.RS1_Pass
        x['RS2_Fail'] = x.RS1_Fail-x.RS2_Pass
        check =0

        if(len(x)>0):
            i=1
            for step in ['FS','RS1','RS2']:
                
                labels=[]
                values =[]
                colors=[]
                if(step == 'FS'):
                    lot_recovery_step_col = 'FS_Result'
                    
                    for idx,sbin in enumerate (x.FS_SBIN.unique()):
                        if(sbin=='SBINpass'):
                            colors.append('green')
                        else:
                            colors.append(lst_color[idx])
                            
                        labels.append(sbin)
                        value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                        values.append(value)
                else:
                    lot_recovery_step_col = step+'_Fail'
                    pass_recovery =step +'_Pass'
                    for idx, sbin in enumerate(x.FS_SBIN.unique()):
                
                        labels.append(sbin)
                        if(sbin=='SBINpass'):
                            colors.append('green')
                            value = x[x.FS_SBIN==sbin][pass_recovery].values[0]
                            values.append(value)
                        else:
                            value = x[x.FS_SBIN==sbin][lot_recovery_step_col].values[0]
                            values.append(value)
                            colors.append(lst_color[idx])
#                 print(labels,values)
                step_result = step+'_Result'
                result = x[step_result].sum()
                step_rs.append(step)
                step_num.append(result)
                fig.add_trace(go.Pie(labels=labels, values=values,marker_colors=colors,text = values),1,i)
                i+=1
            title = 'Rescreen Overall Lot Recovery for '+status+' testProgram'
            fig.update_layout(title= title)
            fig.update_traces(textposition='inside', textfont_size=14)
            fig.add_trace(go.Table(header=dict(values=step_rs),cells=dict(values=step_num)),1,4)
            # for idx in range(len(trace)):
            #     fig.add_trace(trace[idx],2,idx+1)
            fig.update_layout(height=600, width=1400)
            lst_fig.append(fig)
        
    return lst_fig
