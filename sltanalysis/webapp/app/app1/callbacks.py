# import pandas as pd
import plotly
from config import Config
import sys
sys.path.append(Config.path_project)
import analyzer
from analyzer.slt_plot_report import *
from analyzer.slt_plot_rescreen import *
from webapp.app.data.data import Data

# from app.utils.slt_plot_report import *
# from app.utils.slt_plot_rescreen import *

import json
# import datetime as dt
# import random
import dash                              # pip install dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input
from dash_extensions import Lottie       # pip install dash-extensions
import dash_bootstrap_components as dbc
import sqlite3
from pandas import DataFrame
import os
from datetime import date

pd.options.mode.chained_assignment = None  # default='warn'


# from app.dash_shared import shared_dash_nav_links
import time

label= 'label_SBin'

def register_callbacks(app):
    ### update time 
    @app.callback(
    # app.clientside_callback(
        Output('name_max_day', 'children'),
        Input('interval-component', 'n_intervals'))
                
    def update_time(n):
        s = time.time()
        current_time = dt.datetime.now() 
        name_max_day = str(current_time)
        print('time interval ',time.time()-s)
        return html.Span(name_max_day)
    ### read data amkor
    @app.callback(
        Output('data_report', 'data'),
        Input('interval-component', 'n_intervals'),)

    def read_data_report(n):
        s = time.time()
        df_report  = Data.get_data_report()
        # df_report = df_report.to_dict()
        print('===============================UPDATE====================================')
        df_report = df_report.to_dict()
        print('time get data report ',time.time()-s)
        return df_report

    ### read data setup lot
    @app.callback(
        Output('data_setup_lot', 'data'),
        Input('interval-component', 'n_intervals'))     
    def read_data_setup_lot(n):
        s = time.time()
        df_setup_lot = Data.get_data_setup_lot()
        df_setup_lot = df_setup_lot.to_dict()
        print('time get set up ',time.time()-s)
        return df_setup_lot

    #### update data by time selected

    @app.callback(
        Output('dff','data'),
        Output('colors','value'),
        Output('group_col','value'),
        Output('n_part','children'),
        Output('n_pass','children'),
        Output('n_fail','children'),
        Output('list_fail','value'),
        Input('dropdown_report_sbin','value'),
        Input('my-date-picker-start','date'),
        Input('my-date-picker-end','date'),
        Input('data_report', 'data')
    )
    def update_data(n_top_sbin,start_date, end_date,df_report):
        s = time.time()
        df_report = pd.DataFrame(df_report)
        dff = df_report.copy()
        dff = dff[(dff['parse_Day']>=start_date) & (dff['parse_Day']<=end_date)]
        list_fail = get_top_fail_sbin(dff,n_top_sbin) 
        n_part = len(dff)
        n_pass = len(dff[dff.Label=='pass'])
        n_fail = len(dff[dff.Label=='fail'])

        if(len(dff['Week'].unique())<15):
            group_col = 'Week'
        else:
            group_col = 'YearMonth'

        dff,top_5sbin_fail = map_SBin(dff,list_fail)
        colors = map_color_sbin(top_5sbin_fail)
        
        dff = dff.to_dict()
        print('time update data ',time.time()-s)
        return dff,colors,group_col,n_part,n_pass,n_fail,list_fail


    ### firt figure:  sbin fail 
    @app.callback(
        Output('count_sbin_fail_chart','figure'),
        # Output('list_fail','value'),
        Input('my-date-picker-start','date'),
        Input('my-date-picker-end','date'),
        Input('data_report', 'data'))

    def update_count_sbin_fail(start_date, end_date,df_report):
        s = time.time()
        df_report = pd.DataFrame(df_report)
        dff = df_report.copy()
        dff = dff[(dff['parse_Day']>=start_date) & (dff['parse_Day']<=end_date)]
        fig = count_fail_sbin(dff) 
        print('count sbin fail ',time.time()-s)
        return fig


    
    ### update env_data ( split key Enviroment)
    @app.callback(
        Output('env_dff','data'),
        Input('my-date-picker-start','date'),
        Input('my-date-picker-end','date'),
        Input('dff','data'),
        
    )
    def update_data_env(start_date, end_date,dff):
        s = time.time()
        dff = pd.DataFrame(dff)

        dff = dff[(dff['parse_Day']>=start_date) & (dff['parse_Day']<=end_date)]
        lst =[]
        for (i,r) in dff.iterrows():
            e = r['Environments']
            e=json.loads(e)
            e['Label'] = r['Label']
            e['label_SBin'] = r['label_SBin']
            e['YearMonthDay'] = r['YearMonthDay']
            e['id'] = r['id']
            lst.append(e)
        env_df = pd.DataFrame.from_dict(lst) 
        env_df['PCP Voltage']=env_df['PCP Voltage'].str.replace(' mV', '')
        env_df['Leak Value']=env_df['Leak Value'].str.replace(' mV', '')

        env_df['Leak Value']=env_df['Leak Value'].replace('None',np.nan)
        env_df['Leak Value']=env_df['Leak Value'].replace('N/A',np.nan)
        env_df['PCP Voltage']=env_df['PCP Voltage'].replace('None',np.nan)
        env_df['PCP Voltage']= env_df['PCP Voltage'].replace('N/A',np.nan)

        env_df['Leak Value'].astype(float)
        env_df['PCP Voltage'].astype(float)
        env_df['Leak Value']=pd.to_numeric(env_df['Leak Value'])  
        env_df['PCP Voltage']=pd.to_numeric(env_df['PCP Voltage'])
        # print('============================')
        # print(env_df.columns)
        env_df = env_df.to_dict()
        print('time envdata ',time.time()-s)
        return env_df


    # show chart for each sbin ------------------------------
    @app.callback(
        Output('sbin_fail_fig','figure'),

        Input('dff','data'),
        Input('sbin_num','value'),
        Input('list_fail','value'),
    )
    def sbin_fail(dff,sbin_top_num,list_fail ):

        dff = pd.DataFrame(dff)
        sbin =list_fail[sbin_top_num-1]
        df_fail=dff[dff['Label']=='fail']
        df_fail_sbin=df_fail[df_fail.SBin==sbin]
        fig= top_fail_by_site(df_fail_sbin,'TesterNumber','HandlerID',colors_handler,True,sbin)
        return fig


    ###pie chart sumary --------------------------------------------------
    @app.callback(
        Output('pie_chart','figure'),
        Input('dff','data'),
        Input('colors','value'),
    )

    def update_pie_sumary_chart(dff, colors):
        dff = pd.DataFrame(dff)
        pie_chart = pie_sumary(dff,label,'Sumary',colors)
        return pie_chart


    #### update handler chart
    @app.callback(
        Output('handler_chart','figure'),
        Input('dff','data'),
        Input('colors','value'),
        Input('dropdown_multi_handler','value')
    )

    def update_handler_chart(dff ,colors,lst_handler):
        dff = pd.DataFrame(dff)
        chart = count_by_col(dff,'HandlerID',label,colors,lst_handler)
        return chart
    ### update handler chart by time
    @app.callback(
        Output('handler_chart_by_time','figure'),
        Input('dff','data'),
        Input('colors','value'),
        Input('group_col','value'),
        Input('dropdown_handler','value'),
    )

    def update_handler_chart_by_time(dff ,colors,group_col,lst_value):
        dff = pd.DataFrame(dff)
        chart = sumary_by_time(dff,group_col,'HandlerID',label,colors,lst_value)
        return chart

    ### update chart_handler_by_site
    @app.callback(
        Output('chart_handler_by_site','figure'),
        Input('dff','data'),   
    )

    def update_testernumber(dff ):
        dff = pd.DataFrame(dff)   
        chart = sumary_site_by_hontech(dff,'TesterNumber','HandlerID',colors_handler,True)
        return chart

    ### update testNumber by handler
    @app.callback(
        Output('site_by_handler','figure'),
        Input('dff','data'), 
        Input('colors','value'),
        Input('dropdown_site_by_handler','value'),  
    )

    def update_testernumber_by_handler(dff,colors, handler):
        dff = pd.DataFrame(dff)   
        df_handler=dff[dff['HandlerID']==handler]
        
        lst_site = df_handler.TesterNumber.unique()
        chart = count_by_col(df_handler,'TesterNumber',label,colors,lst_site)
        title='Count pass/fail for HandlerID: '+handler
        chart.update_layout(title= title)
        return chart
    ### update testNumber by handler by time
    @app.callback(
        Output('site_by_handler_by_time','figure'),
        Input('dff','data'), 
        Input('colors','value'),
        Input('group_col','value'),
        Input('dropdown_site_by_handler_by_time','value'),  
    )

    def update_testernumber_by_time(dff,colors, group_col,handler):
        dff = pd.DataFrame(dff)   
        try:
            df_handler=dff[dff['HandlerID']==handler]
            lst_value = list(df_handler.TesterNumber.unique())
            # print('sumary_by_time tester: ',lst_value)
            # print('dff[HandlerID]',dff['HandlerID'].unique())
            # print(len(df_handler))
            chart = sumary_by_time(df_handler,group_col,'TesterNumber',label,colors,lst_value)
            title='Count pass/fail by time for HandlerID: '+handler
            chart.update_layout(title= title)
        except:
            print('error')
            chart=0
        return chart


    ### update sku sumary
    @app.callback(
        Output('sku_fig_sumary','figure'),
        Input('dff','data'), 
        Input('colors','value'),
        Input('group_col','value'),  
        Input('dropdown_sku_time','value'), 
    )

    def update_sku_by_time(dff,colors,group_col,lst_sku):
        dff = pd.DataFrame(dff) 

        fig = sumary_by_time(dff,group_col,'SKU',label,colors,lst_sku)  
        title='Count pass/fail by time for SKU'
        fig.update_layout(title= title)
        return fig


    ### update sku
    @app.callback(
        Output('sku_fig','figure'),
        Input('dff','data'), 
        Input('colors','value'),
        Input('dropdown_sku','value'),  
    )

    def update_sku(dff,colors,dropdown_sku):
        dff = pd.DataFrame(dff)   
        df_sku = dff[dff['SKU']==dropdown_sku]
        name_='SKU '+dropdown_sku
        fig = pie_sumary(df_sku,label,name_,colors)
        return fig




    ### update leak value 
    @app.callback(
        Output('leaky_value_fig','figure'),
        Input('env_dff','data'), 
        Input('colors','value'),
        
    )

    def update_leaky_value(env_dff,colors):
        env_dff = pd.DataFrame(env_dff)   
        fig = count_by_col(env_dff,'Leak Value',label,colors,None)
        return fig

    ### update pcp_voltage_fig
    @app.callback(
        Output('pcp_voltage_fig','figure'),
        Input('env_dff','data'), 
        Input('colors','value'),
        
    )

    def update_pcp_voltage(env_dff,colors):
        env_dff = pd.DataFrame(env_dff)  
        fig = count_by_col(env_dff,'PCP Voltage',label,colors,None)
        return fig
