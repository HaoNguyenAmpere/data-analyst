import pandas as pd
import plotly
from config import Config

import sys
sys.path.append(Config.path_project)
import analyzer
from analyzer.slt_plot_report import *
from analyzer.slt_plot_rescreen import *

import json
import datetime as dt
import random
import dash                              # pip install dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input
# from app.utils.slt_plot_rescreen import *
from dash_extensions import Lottie       # pip install dash-extensions
import dash_bootstrap_components as dbc
import plotly.io as pio
import sqlite3
from pandas import DataFrame
import os
from datetime import date
# from callbacks import *
pd.options.mode.chained_assignment = None  # default='warn'
# from app import app
# from .data.data import Data
from app.data.data import Data
from app.dash_shared import shared_dash_nav_links
#---------------------------------------------------
import time

s =time.time()

df_tmp = Data.get_data_report()
df_tmp_history = Data.get_data_history()
df_set_up = Data.get_data_setup_lot()

df_tmp_history=df_tmp_history[df_tmp_history.TestStep.isin(['FS','RS1','RS2'])]
df_set_up['WaferLotID']= df_set_up.apply(lambda x: split_lot(x['Number']), axis=1)
df_set_up = df_set_up[df_set_up['WaferLotID'].isin(lst_correclation) == False]

sku_unique = ['64.30','80.30']
lst_pass=[1,120,130]
label= 'label_SBin'
label_isChar=True
handler_unique = df_tmp.HandlerID.unique()
lot_unique = [value for value in list(df_set_up.Number.unique()) if value in list(df_tmp_history.LotNumber.unique())] #df_set_up.Number.unique()
print(time.time()-s)

#------------------------------------------------------------layout -----------------------------------------

def plotly_sample_app():
    layout1 = html.Div(
        [
            dcc.Store(id='data_setup_lot'),
            # dcc.Store(id='lst_set_up_lot'),
            dcc.Store(id='data_report'),
            # dcc.Store(id='list_fail'),
            dcc.Store(id='dff'),
            dcc.Store(id='env_dff'),
            # dcc.Store(id='colors'),
            # dcc.Store(id='group_col'),


            # dcc.Store(id='n_part'),
            # dcc.Store(id="aggregate_data"),
            # empty Div to trigger javascript file for graph resizing
            # html.Div(id="output-clientside"),
            html.Div(
                [
                    # html.Div(
                    #     [
                    #         html.Img(
                    #             src=app.get_asset_url("./ampere.png"),
                    #             id="ampere",
                    #             style={
                    #                 "height": "60px",
                    #                 "width": "auto",
                    #                 "margin-bottom": "25px",
                    #             },
                    #         )
                    #     ],
                    #     className="one-third column",
                    # ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H3(
                                        "Data Analyst",
                                        style={"margin-bottom": "0px"},
                                    ),
                                    html.H5(
                                        "Production Overview", style={"margin-top": "0px"}
                                    ),
                                ]
                            )
                        ],
                        className="one-half column",
                        id="title",
                    ),
                    html.Div(
                        [
                            html.A(
                                html.Button("About us", id="learn-more-button"),
                                href="https://amperecomputing.com/",
                            )
                        ],
                        className="one-third column",
                        id="button",
                    ),
                ],
                id="header",
                className="row flex-display",
                style={"margin-bottom": "25px"},
            ),
            html.Div(
                [   
                    html.Div([
                        html.Div(
                                        [html.P("New Latest Update Time"),html.P(id="name_max_day")],
                                        
                                        className="mini_container",
                                    ),
                    
                        # html.Div([
                        #     html.P('New Latest Update from '),
                        #     html.Div(id = 'name_max_day',className="control_label"),]),
                        
                        dcc.Interval(
                            id='interval-component',
                            interval=1*300000, # in milliseconds
                            n_intervals=0
                            ),
                        html.Div([
                        html.P("Select time"), 
                        dcc.DatePickerSingle(
                            id='my-date-picker-start',
                            date=date(2021, 7, 19),
                            
                        ),
                        dcc.DatePickerSingle(
                            id='my-date-picker-end',
                            date=date(2021, 7, 25),
                            
                        ),
                        ],className="mini_container",),
                        html.Div([
                        html.P("Select number sbin to show"), 
                        dcc.Dropdown(
                            id='dropdown_report_sbin',
                            options=[
                                {'label': 'top 1', 'value': 1},
                                {'label': 'top 2', 'value': 2},
                                {'label': 'top 3', 'value': 3},
                                {'label': 'top 4', 'value': 4},
                                {'label': 'top 5', 'value': 5},
                                {'label': 'all sbin', 'value': 0}
                            ],
                            value=0,
                            # placeholder="Select handler",
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                            # className="dcc_control",
                            ),
                        ],className="mini_container" ,id="cross-filter-options",),
                        ],className="pretty_container_left"),
                    
                    
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [html.H6(id="n_part"), html.P("Number Parts")],
                                        id="wells",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="n_pass"), html.P("Pass")],
                                        id="gas",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="n_fail"), html.P("Fail")],
                                        id="oil",
                                        className="mini_container",
                                    ),
                            
                                ],
                                id="info-container",
                                className="row container-display",
                            ),
                            html.Div(
                                [
                                    # dcc.Graph(id="count_sbin_fail_chart")
                                    dcc.Loading(id="loading-1",
                                        children=[html.Div(dcc.Graph(id='count_sbin_fail_chart'))], type="circle",
                                    ),
                                ],
                                id="countGraphContainer",
                                className="pretty_container",
                            ),
                        ],
                        id="right-column",
                        className="eight columns",
                    ),
                ],
                className="row flex-display",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            # dcc.Graph(id="pie_chart")
                            dcc.Loading(id="loading-2",
                                        children=[html.Div(dcc.Graph(id='pie_chart'))], type="circle",
                                    ),
                        ],
                        className="pretty_container seven columns",
                    ),
                    html.Div([
                        dbc.FormGroup(
                        [
                            dbc.Label("Enter num of top sbin fail  "),
                            dcc.Input(id="sbin_num",type="number", min=1,value=1,max=5 ,step=1),
                            # dbc.FormText("Type something in the box above"),
                        ]),
                        # dcc.Graph(id="sbin_fail_fig"),
                        dcc.Loading(id="loading-3",
                                        children=[html.Div(dcc.Graph(id='sbin_fail_fig'))], type="circle",
                                    ),
                        
                    ],className="pretty_container five columns",),
                ],
                className="row flex-display",
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Dropdown(
                            id='dropdown_multi_handler',
                            options=[{'label':name, 'value':name} for name in handler_unique],
                            value=['HONTECHSLT-13', 'HONTECHSLT-14'],
                            multi=True,
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                        
                            ),
                        # dcc.Graph(id='handler_chart', figure={}, config={'displayModeBar': True}),
                        dcc.Loading(id="loading-4",
                                    children=[html.Div(dcc.Graph(id='handler_chart'))], type="circle",
                                    ),
                        ],
                        className="pretty_container seven columns",
                    ),
                    html.Div(
                        [
                            dcc.Dropdown(
                            id='dropdown_handler',
                            options=[{'label':name, 'value':name} for name in handler_unique],
                            value=[handler_unique[1]],
                            multi=True,
                            # placeholder="Select handler",
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                            ),
                        # dcc.Graph(id='handler_chart_by_time', figure={}, config={'displayModeBar': True}),
                        dcc.Loading(id="loading-5",
                                    children=[html.Div(dcc.Graph(id='handler_chart_by_time'))], type="circle",
                                    ),
                        ],
                        className="pretty_container five columns",
                    ),
                    
                ],
                className="row flex-display",
            ),

            html.Div(
                [
                # dcc.Graph(id='chart_handler_by_site', figure={}, config={'displayModeBar': True}),
                dcc.Loading(id="loading-6",
                                    children=[html.Div(dcc.Graph(id='chart_handler_by_site'))], type="circle",
                                    ),
                ],
                className="pretty_container ",
                # id = 'countGraphContainer',
            ),
            html.Div(
                [
                    

                    html.Div(
                        [
                            dcc.Dropdown(
                            id='dropdown_site_by_handler',
                            options=[{'label':name, 'value':name} for name in handler_unique],
                            value=handler_unique[0],
                            # multi=True,
                            # placeholder="Select handler",
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                            ),
                        # dcc.Graph(id='site_by_handler', figure={}, config={'displayModeBar': True}),
                        dcc.Loading(id="loading-7",
                                    children=[html.Div(dcc.Graph(id='site_by_handler'))], type="circle",
                                    ),
                        ],
                        className="pretty_container five columns",
                    ),
                    html.Div(
                        [
                            dcc.Dropdown(
                            id='dropdown_site_by_handler_by_time',
                            options=[{'label':name, 'value':name} for name in handler_unique],
                            value=handler_unique[0],
                            # multi=True,
                            # placeholder="Select handler",
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                            ),
                        # dcc.Graph(id='site_by_handler_by_time', figure={}, config={'displayModeBar': True}),
                        dcc.Loading(id="loading-8",
                                    children=[html.Div(dcc.Graph(id='site_by_handler_by_time'))], type="circle",
                                    ),
                        ],
                        className="pretty_container seven columns",
                    ),
                    
                ],
                className="row flex-display",
            ),

            html.Div(
                [
                    html.Div(
                        [
                        dcc.Dropdown(
                        id='dropdown_sku_time',
                        options=[{'label':name, 'value':name} for name in sku_unique],
                        value=[sku_unique[-1]],
                        multi =True,
                        placeholder="Select sku",
                        style=dict(
                            # width='60%',
                            verticalAlign="middle"
                            )
                        ),
                    # dcc.Graph(id='sku_fig_sumary', figure={}, config={'displayModeBar': True}),
                    dcc.Loading(id="loading-9",
                                    children=[html.Div(dcc.Graph(id='sku_fig_sumary'))], type="circle",
                                    ),
                    ],className="pretty_container five columns",
                    ),
                    html.Div(
                        [
                            dcc.Dropdown(
                        id='dropdown_sku',
                        options=[{'label':name, 'value':name} for name in sku_unique],
                        value=sku_unique[-1],
                        placeholder="Select sku",
                        style=dict(
                            width='60%',
                            verticalAlign="middle"
                            )
                        ),
                            
                    # dcc.Graph(id='sku_fig', figure={}, config={'displayModeBar': True}),
                    dcc.Loading(id="loading-10",
                                    children=[html.Div(dcc.Graph(id='sku_fig'))], type="circle",
                                    ),
                    ],
                        className="pretty_container seven columns",
                    ),
                    
                ],
                className="row flex-display",
            ),

            html.Div(
                [
                # dcc.Graph(id='leaky_value_fig', figure={}, config={'displayModeBar': True}),
                dcc.Loading(id="loading-11",
                                    children=[html.Div(dcc.Graph(id='leaky_value_fig'))], type="circle",
                                    ),
                ],
                className="pretty_container ",
                # id = 'countGraphContainer',
            ),
            html.Div(
                [
                # dcc.Graph(id='pcp_voltage_fig', figure={}, config={'displayModeBar': True}),
                dcc.Loading(id="loading-12",
                                    children=[html.Div(dcc.Graph(id='pcp_voltage_fig'))], type="circle",
                                    ),
                ],
                className="pretty_container ",
                # id = 'countGraphContainer',
            ),


        # dcc.Store(id='data_setup_lot'),
        # dcc.Store(id='lst_set_up_lot'),
        # dcc.Store(id='data_report'),
        # dcc.Store(id='list_fail'),
        # dcc.Store(id='dff'),
        # dcc.Store(id='env_dff'),
        # dcc.Store(id='colors'),
        # dcc.Store(id='group_col'),
        ],
        id="mainContainer",
        style={"display": "flex", "flex-direction": "column"},
    )
    return layout1



layout = html.Div(
    id='app_1_page_layout',
    children=[
        # shared_dash_nav_links(),
        plotly_sample_app(),
        # other layout functions can go here.
    ]
)

