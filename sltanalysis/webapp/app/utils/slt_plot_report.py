
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import datetime as dt
from plotly.offline import plot
import random
import plotly.io as pio
import json
import sqlite3
# from slt_plot_rescreen import *
lst_correclation = ['TAAF54','TA7H13','TA3T23','TAAF54','TAAF51','TAAF57','TAAM50','TARW31']

colors_handler={
    'HONTECHSLT-13': 'pink',
    'HONTECHSLT-14': 'green',
    'HONTECHSLT-15': 'blue',
    'HONTECHSLT-16': 'yellow',
}

dict_name_sbin ={1:'Pass',
5:'SBIN#5 MESH Error',
7:'SBIN#7 GIC Error',
10:'SBIN#10 DDR-Init',
15:'SBIN#15 DDR-SCP/ATF',
20:'SBIN#20 GSA-Error',
25:'SBIN#25 CCIX-link Errors',
27:'SBIN#27 CCIX-Data-Error-After-OS',
30:'SBIN#30 PCIE-Link-In-ATF',
35:'SBIN#35 PCIE-Link-CentOS',
40:'SBIN#40 AER-Error',
45:'SBIN#45 FIO/IPERF-Error',
50:'SBIN#50 Temperature-Error',
52:'SBIN#52 Thermo Shutdown',
53:'SBIN#53 Power too high when Tj is OK',
55:'SBIN#55 Kernel-CentOS-Error',
60:'SBIN#60 Hardware CentOS-Cache-Error',
61:'SBIN#61 Hardware CentOS-SM-Error',
63:'SBIN#63 eFuse Reading Error',
65:'SBIN#65 SKU-Error',
70:'SBIN#70 CentOS-Boot',
75:'SBIN#75 CentOS-Crash',
80:'SBIN#80 CentOS-Hang',
85:'SBIN#85 PMPRO-WDT',
90:'SBIN#90 Testable Non Bin ',
95:'SBIN#95 Untestable Non Bin',
100:'SBIN#100 Time Out',
120:'SBIN#120 SKU_SKEW_FF',
130:'SBIN#130 SKU_SKEW_SS'}

def split_lot(x):
    result = x.split('.')[0]
    if(len(x.split('.'))<2):
        result = x
    return result

def split_sku(x):
    cores = str(x.split('.')[2])
    frequency = str(x.split('.')[3])
    a = cores+'.'+frequency
    return a

def create_sku(x):
    cores = str(x.split('.')[0]).split('x')[1]
    frequency = str(x.split('.')[1]).split('x')[1]
    a = cores+'.'+frequency
    return a

def handle_data(df):
    df.drop_duplicates(keep = 'last', inplace = True)
    lst_pass=[1,120,130]
    df.SBin=df.SBin.astype(int)

    # df['StartTime'] = pd.to_datetime(df['StartTime'])
    # df.StartTime =df.StartTime.dt.tz_convert('Asia/Seoul')
    df['YearMonthDay'] = pd.to_datetime(df['StartTime']).apply(lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month,day=x.day))
    max_day=df['YearMonthDay'].values.max()
    min_day=df['YearMonthDay'].values.min()
    name_day = 'from '+min_day+' to '+max_day
    df['YearMonth'] = pd.to_datetime(df['StartTime']).apply(lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
    df['StartTime']=pd.to_datetime(df['StartTime'])

    df['YearMonthDay'] =  pd.to_datetime(df['YearMonthDay'])
    # df['Day'] = df['YearMonthDay'] 
    df['parse_Day'] = pd.to_datetime(df['StartTime']).apply(lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month,day=x.day))
    df['Day'] = pd.to_datetime(df['StartTime']).apply(lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month,day=x.day))

    df=df.sort_values(by=['StartTime'],ascending=True)
    df['Week'] = df['YearMonthDay'].apply(lambda x: dt.datetime.strftime(x,'%W'))
    df['Label'] = df.apply(lambda x: myfunc(x['SBin'],lst_pass), axis=1)
    df.HBin=df.HBin.astype(str)
    df['TestStep'] = df.apply(lambda x: convert_testep(x['ReportFile']), axis=1)
    df['TestStep'] = df['TestStep'].replace(['RS', 'RSS'], 'RS1')
    df['YearMonthDay'] = pd.to_datetime(df.YearMonthDay, format='%Y-%m-%d')
    df['YearMonthDay'] = df['YearMonthDay'].dt.strftime('%Y-%m-%d')
    df=df.replace('HONTECLSLT-14', 'HONTECHSLT-14')
    df=df.replace('HONTECH-14', 'HONTECHSLT-14')
    df=df[df['HandlerID']!='H-1000']
    df=df[df['TesterNumber'] != 7]
    # lot_step=[]
    # for index, row in df.iterrows():
    #         lot_step.append(str(row['LotNumber'])+'_'+str(row['TestStep']))
    # df['LotStep']=lot_step
    df['LotStep']=df['LotNumber'] +'_'+df['TestStep']
    
    return df

def handle_data_report(df_report,lst_set_up_lot):
    df_report.drop_duplicates(keep = 'last', inplace = True)

    df_report.SBin=df_report.SBin.astype(int)
    df_report['SBinName'] =df_report['SBin']
    df_report = df_report.replace({"SBinName": dict_name_sbin})
    df_report=df_report[df_report['HandlerID']!='H-1000']
    # df_report['parse_Day'] = pd.to_datetime(df_report['Day'], format='%Y-%m-%d')
    df_report['parse_Day'] = pd.to_datetime(df_report['parse_Day']).dt.date
    df_report['Day'] = pd.to_datetime(df_report['Day']).dt.date
    df_report.HBin=df_report.HBin.astype(str)
    df_report=df_report.sort_values(by=['StartTime'],ascending=True)
    df_report.Cores = df_report.Cores.replace('64', '0x64')
    df_report.Cores = df_report.Cores.replace('80', '0x80')
    df_report.Frequency = df_report.Frequency.replace('30', '0x30')
    df_report.Frequency = df_report.Frequency.replace('33', '0x33')
    df_report=df_report.replace('HONTECLSLT-14', 'HONTECHSLT-14')
    df_report=df_report.replace('HONTECH-14', 'HONTECHSLT-14')
    df_report['time'] =  pd.to_datetime(df_report['StartTime'])

    df_report['WaferLotID']= df_report.apply(lambda x: split_lot(x['LotNumber']), axis=1)
    df_report = df_report[df_report['WaferLotID'].isin(lst_correclation) == False]

    df_report = df_report[df_report.LotNumber.isin(lst_set_up_lot)]
    data=[]
    for lot in df_report.LotNumber.unique():
        df_lot = df_report[df_report.LotNumber==lot]
        if(len(lot.split('.'))>2):
            df_lot['SKU']= df_lot.apply(lambda x: split_sku(x['LotNumber']), axis=1)
            data.append(df_lot)
        else:
            df_lot['SKU']= df_lot['Cores']+'.'+df_lot['Frequency']
            df_lot['SKU']= df_lot.apply(lambda x: create_sku(x['SKU']), axis=1)
            data.append(df_lot)

    data =pd.concat(data)
    data = data[~data.SKU.isnull()]
    data = data[data.SKU !='None.None']
    return data


def map_SBin_new(df):
    max_fail = df[df['Label']=='fail']['SBin'].value_counts().index[0]
    df['label_SBin']='other_fail'
    df.loc[df['Label']=='pass','label_SBin']='pass'
    df.loc[df['SBin']==max_fail,'label_SBin']= 'max_fail: '+str(max_fail)
    return df,max_fail

def handle_data_history(df,lst_set_up_lot):
    df.drop_duplicates(keep = 'last', inplace = True)
    df.HBin=df.HBin.astype(str)
    df=df.sort_values(by=['StartTime'],ascending=True)
    df,max_fail=map_SBin_new(df)
    df['time'] =  pd.to_datetime(df['StartTime'])
    df['LastStep']=0
    df['lbl']=df['SBin']
    df.loc[df['Label']=='pass','lbl']='pass'
    df['lbl']=df['lbl'].astype(str)
    df['parse_StartTime']=pd.to_datetime(df['StartTime'])
    df['WaferLotID']= df.apply(lambda x: split_lot(x['LotNumber']), axis=1)
    df = df[df['WaferLotID'].isin(lst_correclation) == False]
    df = df.sort_values(by=['parse_StartTime'],ascending=True)
    df = df.drop_duplicates(subset=['Serial',  'TestStep', 'LotNumber','HandlerID'], keep='last')
    df['WaferLotID']= df.apply(lambda x: split_lot(x['LotNumber']), axis=1)
    df = df[df['WaferLotID'].isin(lst_correclation) == False]

    df = df[df.LotNumber.isin(lst_set_up_lot)]
    data=[]
    for lot in df.LotNumber.unique():
        df_lot = df[df.LotNumber==lot]
        if(len(lot.split('.'))>2):
            df_lot['SKU']= df_lot.apply(lambda x: split_sku(x['LotNumber']), axis=1)
            data.append(df_lot)
        else:
            df_lot['SKU']= df_lot['Cores']+'.'+df_lot['Frequency']
            df_lot['SKU']= df_lot.apply(lambda x: create_sku(x['SKU']), axis=1)
            data.append(df_lot)

    data =pd.concat(data)
    data = data[~data.SKU.isnull()]
    df = data[data.SKU !='None.None']
    return df

def handle_setup_lot(df_setup_lot):
    df_setup_lot.drop_duplicates(keep = 'last', inplace = True)
    df_setup_lot.CreatedOn=pd.to_datetime(df_setup_lot.CreatedOn)
    df_setup_lot.CreatedOn =df_setup_lot.CreatedOn.dt.tz_convert('Asia/Seoul')

    df_setup_lot['Day'] = pd.to_datetime(df_setup_lot['CreatedOn']).apply(lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month,day=x.day))
    df_setup_lot['Day'] = pd.to_datetime(df_setup_lot['Day'], format='%Y-%m-%d')
    df_setup_lot['Day'] = df_setup_lot['Day'].dt.strftime('%Y-%m-%d')
    df_setup_lot['Handler'] = df_setup_lot['Handler'].str.upper()
    df_setup_lot['Handler']=df_setup_lot['Handler'].replace('NTECHSLT-13','HONTECHSLT-13')

    df_setup_lot.Handler = df_setup_lot.Handler.replace('HONTCHSLT-13','HONTECHSLT-13')
    df_setup_lot.Handler = df_setup_lot.Handler.replace('HONTECLSLT-14','HONTECHSLT-14')
    df_setup_lot.Handler = df_setup_lot.Handler.replace('HONTECTSLT-14','HONTECHSLT-13')
    df_setup_lot.Handler = df_setup_lot.Handler.replace('HONTECHST-13','HONTECHSLT-13')
    df_setup_lot['parse_CreatedOn'] = pd.to_datetime(df_setup_lot['CreatedOn'])
    # df_setup_lot = df_setup_lot[df_setup_lot['WaferLot'].isin(lst_correclation) == False]
    df_setup_lot['WaferLotID']= df_setup_lot.apply(lambda x: split_lot(x['Number']), axis=1)
    df_setup_lot = df_setup_lot[df_setup_lot['WaferLotID'].isin(lst_correclation) == False]
    return df_setup_lot


####-----convert fig to html----
def convert_fig_to_html(fig):
    pio.write_html(fig,'file.html')
    html=open("file.html", "r")
    html=html.read()
    return html
def add_lst_html(report,lst_html):
    for i in lst_html:
        report=report+i
    return report

### lay list index ma2 co1 value cua3 column thay doi
def get_lst_index_change(df,col):
    dic=df.ne(df.shift()).apply(lambda x: x.index[x].tolist())
    lst=dic[col]
    return lst
### lay lst ngay ma co value cua column thay doi
def get_lst_day_change(df,lst_change,col):
    df_= df.loc[lst_change]
    a=df_[col]
    return a

### ---------------------------- GET TESTSTEP FROM REPORTFILE ---------------------- ####
# Input: a value of reportfile
# output: teststep

def convert_testep(i): 
    if '_FS_' in i:
        test_step = 'FS'
    elif '_RS_' in i or '_RSS_' in i or '_RS1_' in i:
        test_step = 'RS1'
    elif '_RS2_' in i:
        test_step = 'RS2'
        
    else: 
        test_step='N/A'
    return test_step



# count_fail_sbin(dff,n_top_sbin) 
def get_top_sbin_history(df,num_top):
    df_fail=df[df.Label=='fail']
    list_sbin_fail=list(df_fail.SBin.value_counts().index)
    lst_top_fail = list_sbin_fail[:num_top]
    if(num_top==0):
        lst_top_fail = list_sbin_fail
    return lst_top_fail

def map_SBin_history(df,lst_top_fail):
    max_fail = df[df['Label']=='fail']['SBin'].value_counts().index[0]
    df['lbl_SBin']='other_fail'
    df.loc[df['Label']=='pass','lbl_SBin']='pass'
    lst_lbl=[]
    for i,sbin in enumerate(lst_top_fail):
        top_sbin = 'fail top '+str(i+1)+': '+str(sbin)
        df.loc[df['SBin']==sbin,'lbl_SBin']= top_sbin
        lst_lbl.append(top_sbin)
    return df,lst_lbl
### ---------------------------- CONVERT SBIN TO RESULT: PASS/FAIL  ---------------------- ####
# Input: SBIN: 1,2,3,10,120,130,...
# output: PASS/FAIL

def map_color_sbin(lst_lbl):
    colors={}
    lst_color = ['red','pink','purple','blue','brown','aqua','pink','bisque','brown','purple','coral','darkturquoise',
               'burlywood','cadetblue','violet','navy','chocolate','cornsilk',
               'crimson','cyan','darkcyan','darkgoldenrod','aquamarine','gainsboro','orange']
    colors['pass'] = 'green'
    colors['other_fail'] ='yellow'
    for sbin,color in zip (lst_lbl,lst_color):
        colors[sbin]= color
    return colors
### ---------------------------- CONVERT SBIN TO RESULT: PASS/FAIL  ---------------------- ####
# Input: SBIN: 1,2,3,10,120,130,...
# output: PASS/FAIL

def myfunc(sbin,lst_pass):
    # print('TYPE:',type(sbin))
    if(sbin in lst_pass):
        status='pass'
    else: 
        status='fail'
    return status

### ---------------------------- CONVERT SBIN TO RESULT: PASS/ORDERFAIL/MAXFAIL ---------------------- ####
# Input: SBIN: 1,2,3,10,120,130,...
# output:  PASS/ORDERFAIL/MAXFAIL

def map_SBin(df,lst_top_fail):
    max_fail = df[df['Label']=='fail']['SBin'].value_counts().index[0]
    df['label_SBin']='other_fail'
    df.loc[df['Label']=='pass','label_SBin']='pass'
    lst_lbl=[]
    for i,sbin in enumerate(lst_top_fail):
        top_sbin = 'fail top '+str(i+1)+': '+str(sbin)
        df.loc[df['SBin']==sbin,'label_SBin']= top_sbin
        lst_lbl.append(top_sbin)
    return df,lst_lbl

### ---------------------------- compute percent to show in bar chart ---------------------- ####
# Input: list [[1,2,3],[5,1,3]] => shape=[2,3]
#[[1,2,3]
#[5,1,3]]
# output:  result = 
#[[1/6,2/3,3/6]
#[5/6,1/3,3/6]].T

def percent_compute(lst):
    lst=np.array(lst)
    s=[]
    results=[]
    for j in range(lst.shape[1]):
        lst_each_label=lst[:,j]
        s.append(sum(lst_each_label))
        results.append([(el/s[j])*100 for el in lst[:,j]])

    return np.array(results).T


### ---------------------------- show pie_sumarry for label pass/fail ---------------------- ####
# input: df,name of label , colors, name of chart
# output: a pie chart sumary pass/fail 

def pie_sumary(df,name_lbl,name_,lst_colors):
    
    colors=[]
    for i in df[name_lbl].value_counts().sort_index(ascending=True).index:
        colors.append(lst_colors[i])

    df_sort=df[name_lbl].value_counts().sort_index(ascending=True)
    fig = go.Figure(data=[go.Pie(labels = df_sort.index,
                        values = df_sort.values,text = (df_sort.values))])
    fig.update_layout(title_text = name_+' '+name_lbl)
    fig.update_traces(marker=dict(colors=colors))
    fig.update_traces(textposition='inside', textfont_size=14)
    return fig



### ---------------------------- count pass/fail by each column---------------------- ####
# input:
def count_by_col(df,col_name,label,lst_colors,lst_value):

    check =False
    df_new = df.copy()
    w = len(df[col_name].unique())/30

    if(col_name not in ['Leak Value' ,'PCP Voltage'] ):
        df =df_new.loc[df_new[col_name].isin(lst_value)]
        check_w=False
    else:
        check_w =True

    data=[]
    colors=[]
    y_axis=[]
    fig = make_subplots(specs=[[{"secondary_y": True}]])
 
    # print('........................................#######no no no##########...................................................')
    # print(col_name)
    for i in df[label].value_counts().sort_index(ascending=check).index:
        colors.append(lst_colors[i])
    # print(df[label].value_counts().sort_index(ascending=check).index)
    for i,color in zip(df[label].value_counts().sort_index(ascending=check).index,colors):
        # print(i,color)
        df_=df[df[label]== i]
        key_values = df_[col_name].value_counts().sort_index(ascending=True)
        # print('key_values',key_values)
        df_col=df[col_name].value_counts().sort_index(ascending=True)
        percent=[]
        # print('len  ',len(key_values))
        
        if(len(key_values)>0):
            for j,k in zip(key_values.index,key_values.values):
                percent.append((float(k)*1.0/float(df_col[j]))*100.0)
                if(check_w==True):#[f"{p:.0f}%" for p in percent ],
                    trace=go.Bar(name= str(i),x=key_values.index, y=key_values.values,marker_color=color,text=key_values.values,textposition="outside")
                else:
                    trace=go.Bar(name= str(i),x=key_values.index, y=key_values.values,marker_color=color,text=key_values.values,textposition="outside",width=0.2)
                y_axis.append(max(trace.y))
            fig.add_trace( trace,secondary_y=False)
    
    name=[]
    count=[]
    count_percent=[]
    for i in df[col_name].value_counts().sort_index(ascending=True).index:
        name.append(i)
        df_col=df[df[col_name]==i]
        df_col_pass=df_col[df_col['Label']=='pass']
        count.append(round((len(df_col_pass)/len(df_col))*100,2))
        count_percent.append(str(round((len(df_col_pass)/len(df_col))*100,2))+'%')

    fig.add_trace(go.Scatter(name='yield',x=name,y=count,text=count_percent,line=dict(color='pink'), mode='lines+markers+text'),secondary_y=True)
    title='Count pass/fail by '+col_name
    fig.update_layout(title= title,barmode='stack',
                      xaxis_title= col_name,
                    yaxis_title="count")
    
    y_max=sum(np.unique(y_axis))*1.2
    fig.update_yaxes(title_text="count", secondary_y=False)
    fig.update_yaxes(title_text="percent", secondary_y=True)
    fig.update_yaxes(range=[0, y_max],title_text="count",secondary_y=False)
    fig.update_yaxes(range=[0, 110],title_text="percent",secondary_y=True)
    # fig.update_layout(autosize=False)
    fig.layout.yaxis2.showgrid=False
    
    return fig






### ---------------------------- sumary pass/fail theo time (tháng/ tuần) của tất cả giá trị trong col---------------------- ####
# input:
# output:
def sumary_by_time(df,group_col,key_col,lbl_col,colors,lst_value):
    fig=0

    df = df.loc[df[key_col].isin(lst_value)]

    if(len(df)>0):
        df.sort_values(by=[group_col],inplace=True)
        month=df[group_col].unique()
        lst_count=[]
        lst_name=[]
        lst_colors=[]
        group=[]
        months=[]
        cols=0
        for name in df[key_col].value_counts().sort_index(ascending=True).index:
            for i in month:
                group.append(name)
                months.append(i)
        for lbl in df[lbl_col].value_counts().sort_index(ascending=False).index:
            lst_count_lbl=[]
            lst_colors.append(colors[lbl])
            data=df[df[lbl_col]==lbl]
            for name in df[key_col].value_counts().sort_index(ascending=True).index:
                tmp=data[data[key_col]==name]
                for i in month:
                    month_df=tmp[tmp[group_col]==i]
                    lst_count_lbl.append(len(month_df))
                    cols=cols+1
            lst_count.append(lst_count_lbl)
            lst_name.append(lbl)

        fig = go.Figure()
        n_x=[]
        w= len(df[key_col].unique())/15
        for i in range(len(lst_count)):#[f"{p:.0f}%" for p in percent[i] ]
            percent=percent_compute(lst_count)
            fig.add_trace( go.Bar(name=lst_name[i], x=[months,group], y=lst_count[i],marker_color=lst_colors[i],text=lst_count[i],textposition="inside" ,width=0.2))
            n_x.append([months,group])
        title= 'sumary '+key_col+' by time'

        width=len(np.unique(n_x))*200
        # print(fig)
        fig.update_layout(title= title,barmode='relative',xaxis_title= key_col+'/'+group_col,
                        yaxis_title="count")
        
        # fig.update_layout(autosize=False)
        # print(fig)
    else:
        data = ['Empty', [0], [0]]
        # fig = go.Figure().add_annotation(x=2, y=2,text="No Data to Display",font=dict(family="sans serif",size=25,color="crimson"),showarrow=False,yshift=10)
        fig = go.Figure(data)
        
    return fig




### ---------------------------- show fail.pass by time for each value of column name ---------------------- ####
# input:
# output:
def show_monthly(df,key_col,group_col,label_col,colors,lst):
    df = df.loc[df[key_col].isin(lst)]
    color_discrete_map_line=colors
    color_discrete_map=colors
    lst_x=[]
    lst_y=[]
    lst_z=[]
    grouped_df=df
    d_=grouped_df.groupby([group_col,label_col]).size().reset_index()
    d_['percentage'] = grouped_df.groupby([group_col, label_col]).size().groupby(level=0).apply(lambda x: 100 * x / float(x.sum())).values
    d_.columns = [group_col,label_col, 'Counts', 'Percentage']
    d_=d_.sort_values(by=[label_col],ascending=False)
    fig=px.bar(d_, x=group_col, y="Counts", color=label_col,text=d_['Percentage'].apply(lambda x: '{0:1.2f}%'.format(x)),color_discrete_map=color_discrete_map)
    fig2=px.line(d_, x=group_col, y="Counts", color=label_col,color_discrete_map=color_discrete_map_line)
    fig3 = go.Figure(data=fig.data + fig2.data)
    title='show by time for  '+ key_col 
    w = len(d_[group_col].unique())*300
    fig.update_layout(barmode='stack',title=title,xaxis_title= group_col,
                    yaxis_title="count")
    return fig





#####------------------------------UPDATE 16/06 ---------------------------------

### count cac1 part theo tung handler ID va theo tung TesterNumber, 
def sumary_site_by_hontech(df,col_name,label,lst_colors,label_isChar):
    data=[]
    colors=[]
    df_pass=df[df['Label']=='pass']
    df_fail=df[df['Label']=='fail']
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    for i in df[label].value_counts().sort_index(ascending=True).index:
        # print(i,lst_colors[i])
        colors.append(lst_colors[i])

    for i,color in zip(df[label].value_counts().sort_index(ascending=True).index,colors):
        df_=df[df[label]== i]
        key_values = df_[col_name].value_counts().sort_index(ascending=True)
        
        
        df_col=df[col_name].value_counts().sort_index(ascending=True)
        percent=[]
        for k,c in zip(key_values.values,df_col.values):
            percent.append((float(k)*1.0/float(c))*100.0)

        # trace = go.Bar(name= str(i),x=key_values.index, y=key_values.values,marker_color=color,
        #                text=[f"{p:.0f}%" for p in percent ],textposition="outside")

        trace = go.Bar(name= str(i),x=key_values.index, y=key_values.values,marker_color=color,
                       text=key_values.values,textposition="outside")

        fig.add_trace(trace,secondary_y=False)
    
    for idx,handler in enumerate(df[label].value_counts().sort_index(ascending=True).index): 
        df_=df[df[label]==handler]
        name=[]
        percent=[]
        percent_txt=[]
        for i in df_[col_name].value_counts().sort_index(ascending=True).index:
            x=df[df[col_name]==i]
            sum_handler=len(x)
            name.append(i)
            t=df_[df_[col_name]==i]
            t_pass=t[t['Label']=='pass']
            percent.append(round(len(t_pass)/len(t)*100,2))
            percent_txt.append(str(round(len(t_pass)/len(t)*100,2))+'%')
        # print(name,percent)
        trace= go.Scatter(name=handler,x=name,y=percent,text=percent_txt,line=dict(color=colors[idx]),mode='markers+lines+text')
        data.append(trace) 
        fig.add_trace(trace,secondary_y=True)

    y_max = int(int(key_values.values.max())*1.6)
    # print('key max---------------',key_values.values.max())
    fig.update_yaxes(title_text="count",secondary_y=False)
    fig.update_yaxes(title_text="count",secondary_y=False)
    fig.update_yaxes(range=[0, 100],title_text="percent",secondary_y=True)
    fig.layout.yaxis2.showgrid=False
    fig.update_layout(title= 'count HandlerID by site')
    
    return fig


### count so luong part fail theo tung sbin, lay ra top sbin fial nhieu nhat
def count_fail_sbin(df,num_top):
    line={}
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    df_fail=df[df.Label=='fail']
    list_sbin_fail=list(df_fail.SBin.value_counts().index)
    sbin_fail=dict(df_fail.SBin.value_counts())
    d=pd.DataFrame(sbin_fail.items(), columns=['SBin', 'count'])
    sum_count=d['count'].sum()
    lst_x=list(sbin_fail.keys())
    for idx, (key,value) in enumerate (sbin_fail.items()):
        if(idx>0):
            pre_key=lst_x[idx-1]
            line[key]=(value/sum_count)*100+line[pre_key]
        else:
            line[key]=(value/sum_count)*100
    df_line=pd.DataFrame(line.items(), columns=['SBin', 'percent'])
    
    txt=[]
    for t in df_line['percent'].values:
        txt.append(str(round(t,2))+'%')
    df_line['text']=txt
    
    d = d.replace({"SBin": dict_name_sbin})
    fig_bar=px.bar(d, x='SBin', y='count',text="count")
    
    df_line = df_line.replace({"SBin": dict_name_sbin})
    fig_line= px.line(df_line, x="SBin", y="percent",text='text')
    fig_line.update_traces(mode='markers+lines+text',line_color='red')
    fig_line.update_traces(textposition='top center')
    
    sum_line=[]
    y_max=d['count'].values.max()+10
    for i in df_line['percent'].values:
        tmp=(i*110)/y_max
        
    fig.add_trace(fig_bar['data'][0],secondary_y=False)
    fig.update_xaxes(type="category",categoryarray= list_sbin_fail)
    fig.add_trace(fig_line['data'][0],secondary_y=True)

    fig.update_yaxes(range=[0, y_max],title_text="count",secondary_y=False)
    fig.update_yaxes(range=[0, 110],title_text="percent",secondary_y=True)
    fig.layout.yaxis2.showgrid=False
    fig.update_layout(title= 'Fail by SBin')
    fig['layout']['xaxis']['title']='SBin'
    fig['layout']['yaxis']['title']='count'
    fig['layout']['yaxis2']['title']='percent'
    
    lst_top_fail = list_sbin_fail[:num_top]
    if(num_top==0):
        lst_top_fail = list_sbin_fail

    return fig,lst_top_fail

## count so luong fail theo test va handler cua nhung thang trong top sbin fail
def top_fail_by_site(df,col_name,label,lst_colors,label_isChar,top_sbin):
    data=[]
    colors=[]
    if(label_isChar):
        check=False
    else:
        check=True
        
    for i in df[label].value_counts().sort_index(ascending=True).index:
        # print(i,lst_colors[i])
        colors.append(lst_colors[i])

    for i,color in zip(df[label].value_counts().sort_index(ascending=True).index,colors):
        df_=df[df[label]== i]
        key_values = df_[col_name].value_counts().sort_index(ascending=True)
        
        
        df_col=df[col_name].value_counts().sort_index(ascending=True)
        percent=[]
        for k,c in zip(key_values.values,df_col.values):
            percent.append((float(k)*1.0/float(c))*100.0)

        # trace = go.Bar(name= str(i)+' fail',x=key_values.index, y=key_values.values,marker_color=color,
        #                text=[f"{p:.0f}%" for p in percent ],textposition="outside")
        trace = go.Bar(name= str(i)+' fail',x=key_values.index, y=key_values.values,marker_color=color,
                       text=key_values.values,textposition="outside")
        data.append(trace)

    fig = go.Figure(data)
    title='Count part by site for sbin: '+str(top_sbin)
    fig.update_layout(title= title,
                      xaxis_title= col_name,
                    yaxis_title="count")
    return fig


def show_monthly_col(df,col,value_col,group_col,label_col,colors,label_isChar,color_discrete_map_line): 
    lst_fig=[]
    for i in value_col:
        df_col=df[df[col]==i]
        if(len(df_col)>0):
            name_=col+str(i)
            fig = show_monthly(df_col,col,group_col,label_col,colors,color_discrete_map_line)
            title='show by time for  '+ col +' '+i
            fig.update_layout(title=title)
            lst_fig.append(convert_fig_to_html(fig))
    return lst_fig

def show_monthly(df,key_col,group_col,label_col,colors,color_discrete_map_line):

    color_discrete_map=colors
    color_discrete_map_line
    lst_x=[]
    lst_y=[]
    lst_z=[]
    grouped_df=df
    d_=grouped_df.groupby([group_col,label_col]).size().reset_index()
    d_['percentage'] = grouped_df.groupby([group_col, label_col]).size().groupby(level=0).apply(lambda x: 100 * x / float(x.sum())).values
    d_.columns = [group_col,label_col, 'Counts', 'Percentage']
    d_=d_.sort_values(by=[label_col],ascending=False)
    w = int((len(d_[group_col].unique()))*100)
    print('width',w)
    fig=px.bar(d_, x=group_col, y="Counts", color=label_col,text=d_['Percentage'].apply(lambda x: '{0:1.2f}%'.format(x)),color_discrete_map=color_discrete_map)
    fig2=px.line(d_, x=group_col, y="Counts", color=label_col,color_discrete_map=color_discrete_map_line)
    # fig3 = go.Figure(data=fig.data + fig2.data)
    title='show by time for  '+ key_col 
    for idx in range(len(fig['data'])):
        fig['data'][idx].width = 0.1
    # fig.update_traces(width=1)
    fig.update_layout(barmode='stack',title=title,xaxis_title= group_col,
                    yaxis_title="count")
            
    return fig