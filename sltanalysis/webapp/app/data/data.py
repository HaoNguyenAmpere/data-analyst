import pandas as pd
import os
from datetime import date
import numpy as np
import json
import sqlite3
import datetime as dt

# import sys
# sys.path.append(Config.path_project)
# import analyzer
# from analyzer.slt_plot_report import *
# from analyzer.slt_plot_rescreen import *
# from app.utils.slt_plot_report import dict_name_sbin,lst_correclation,myfunc,convert_testep,map_SBin_new
import time 
import pathlib

path = (pathlib.Path(__file__).parent.resolve()).parent
path_db = os.path.join(path.parent ,'DB_SLT.db')
print('path_db ',path_db)

class Data(object):
    @staticmethod
    def get_data_report():
        s =time.time()
        conn = sqlite3.connect(path_db)
        df = pd.read_sql('''SELECT * FROM SLT_REPORT''', conn)
        # df_setup_lot = pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
        # lst_set_up_lot = list(df_setup_lot['Number'].unique())
        # df = handle_data(df)
        # df = handle_data_report(df,lst_set_up_lot)
        # df.to_sql('SLT_REPORT', conn, if_exists='replace', index = False)
        print('time get data report ',time.time()-s)
        return df
    @staticmethod
    def get_data_history():
        s =time.time()
        conn = sqlite3.connect(path_db)
        df = pd.read_sql('''SELECT * FROM SLT_HISTORY''', conn)
        # df_setup_lot = pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
        # lst_set_up_lot = list(df_setup_lot['Number'].unique())
        # df = handle_data(df)
        # df = handle_data_history(df,lst_set_up_lot)
        # df.to_sql('SLT_HISTORY', conn, if_exists='replace', index = False)
        print('time get data history',time.time()-s)
        return df
    @staticmethod
    def get_data_setup_lot():
        s =time.time()
        conn = sqlite3.connect(path_db)
        df = pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
        # df = handle_setup_lot(df)
        # print('time get data setup lot ',time.time()-s)
        # df.to_sql('SETUP_LOT', conn, if_exists='replace', index = False)
        return df
    @staticmethod
    def get_data_rescreen():
        s =time.time()
        conn = sqlite3.connect(path_db)
        df = pd.read_sql('''SELECT * FROM SLT_RESCREEN''', conn)
        return df
    @staticmethod
    def get_available_handler(df) -> list:
        return df['HandlerID'].unique()
    @staticmethod
    def get_available_lot(df) -> list:
        return df['LotNumber'].unique()

"""
# get static data example
import os
DATASETS = os.path.abspath(os.path.dirname(__file__))
df=pd.read_csv(DATASETS+'/data.csv')
"""
# def split_lot(x):
#     result = x.split('.')[0]
#     if(len(x.split('.'))<2):
#         result = x
#     return result

# def split_sku(x):
#     cores = str(x.split('.')[2])
#     frequency = str(x.split('.')[3])
#     a = cores+'.'+frequency
#     return a

# def create_sku(x):
#     cores = str(x.split('.')[0]).split('x')[1]
#     frequency = str(x.split('.')[1]).split('x')[1]
#     a = cores+'.'+frequency
#     return a
