import pandas as pd
import plotly

from app.utils.slt_plot_report import *
import json
import datetime as dt
import random
import dash                              # pip install dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input
from app.utils.slt_plot_rescreen import *
from dash_extensions import Lottie       # pip install dash-extensions
import dash_bootstrap_components as dbc
import plotly.io as pio
import sqlite3
from pandas import DataFrame
import os
from datetime import date
import time 
# from callbacks import *
pd.options.mode.chained_assignment = None  # default='warn'
# from app import app
# from .data.data import Data
from app.app1.data.data import Data
from app.dash_shared import shared_dash_nav_links

# conn = sqlite3.connect('DB_SLT.db')
# df_tmp = pd.read_sql('''SELECT * FROM SLT_REPORT''', conn)
# df_tmp_history = pd.read_sql('''SELECT * FROM SLT_HISTORY''', conn)
# df_tmp_history=df_tmp_history[df_tmp_history.TestStep.isin(['FS','RS1','RS2'])]
# df_set_up = pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
# df_set_up['WaferLotID']= df_set_up.apply(lambda x: split_lot(x['Number']), axis=1)
# df_set_up = df_set_up[df_set_up['WaferLotID'].isin(lst_correclation) == False]

# sku_unique = ['64.30','80.30']
# lst_pass=[1,120,130]
label= 'label_SBin'
# label_isChar=True
# handler_unique = df_tmp.HandlerID.unique()
# lot_unique = [value for value in list(df_set_up.Number.unique()) if value in list(df_tmp_history.LotNumber.unique())] 


def register_callbacks(app):
    # @app.callback(
    #     Output('data_history_app2', 'data'),
    #     Input('interval-component', 'n_intervals'),
    #     Input('lst_set_up_lot_app2', 'value')) 
    # def read_data_history_app2(n,lst_set_up_lot):
    #     conn = sqlite3.connect('DB_SLT.db')
    #     df = pd.read_sql('''SELECT * FROM SLT_HISTORY''', conn)
    #     df = handle_data_history(df,lst_set_up_lot)
    #     df = df.to_dict()
    #     return df
    @app.callback(
        Output('data_history_app2', 'data'),
        Input('interval-component', 'n_intervals'),
        ) 
    def read_data_history_app2(n):
        # conn = sqlite3.connect('DB_SLT.db')
        # df = pd.read_sql('''SELECT * FROM SLT_HISTORY''', conn)
        df = Data.get_data_history()
        df = df.to_dict()
        return df

    # @app.callback(
    #     Output('data_setup_lot_app2', 'data'),
    #     Output('lst_set_up_lot_app2', 'value'),
    #     Input('interval-component', 'n_intervals'))     
    # def read_data_setup_lot_app2(n):
    #     conn = sqlite3.connect('DB_SLT.db')
    #     df_setup_lot=pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
    #     df_setup_lot = handle_setup_lot(df_setup_lot)
    #     # print('len_df_setup_lot',len(df_setup_lot))
    #     lst_set_up_lot = list(df_setup_lot['Number'].unique())
    #     df_setup_lot = df_setup_lot.to_dict()
    #     return df_setup_lot,lst_set_up_lot
    @app.callback(
        Output('data_setup_lot_app2', 'data'),
        Input('interval-component', 'n_intervals'))     
    def read_data_setup_lot_app2(n):
        s = time.time()
        df_setup_lot = Data.get_data_setup_lot()
        df_setup_lot = df_setup_lot.to_dict()
        print('time get set up ',time.time()-s)
        return df_setup_lot



    @app.callback(
        Output('data_rescreen','data'),
        Input('data_setup_lot_app2', 'data'),
        Input('data_history_app2', 'data'),
        Input('interval-component', 'n_intervals'))
    def read_data_screen(df_setup_lot,df,n):
        df = pd.DataFrame(df)
        df_setup_lot = pd.DataFrame(df_setup_lot)
        data_count = count_part_setup_lot(df,df_setup_lot,'count_part_setup_lot_event_.xlsx')
        # print('len_data_count',len(data_count))
        data_count_clean = clean_data_count_event(data_count)
        # print('data_count_clean',len(data_count_clean))
        data_rescreen = sumary_rescreen(df,data_count_clean,'Lot_sumary.xlsx')
        data_rescreen.columns = ['_'.join(col) for col in data_rescreen.columns]

        data_rescreen = data_rescreen.to_dict()
        return data_rescreen


    @app.callback(
        Output('data_rescreen_by_lot', 'data'),
        Input('data_rescreen','data'),
        Input('interval-component', 'n_intervals'))        
    def read_data_by_lot(data_rescreen,n):
        data_rescreen = pd.DataFrame(data_rescreen)
        data_rescreen_by_lot = sumary_rescreen_by_lot(data_rescreen, 'Lot_sumary_new_lot.xlsx')
        data_rescreen_by_lot = data_rescreen_by_lot.dropna(how='all')
        data_rescreen_by_lot = pd.DataFrame(data_rescreen_by_lot).reset_index()
        data_rescreen_by_lot = data_rescreen_by_lot.to_dict()
        return data_rescreen_by_lot


    @app.callback(
        Output('data_rescreen_sbin', 'data'),
        Input('data_rescreen','data'),
        Input('interval-component', 'n_intervals'))
                
    def read_data_rescreen_sbin(data_rescreen,n):
        data_rescreen = pd.DataFrame(data_rescreen)
        data_rescreen_sbin = remove_lot_not_rescreen(data_rescreen)
        data_rescreen_sbin = sumary_rescreen_by_sbin(data_rescreen_sbin, 'Lot_sumary_new_sbin.xlsx')
        # print('data_rescreen_sbin',len(data_rescreen_sbin))
        # print(data_rescreen_sbin.columns)
        data_sbin = split_data_sbin(data_rescreen_sbin)
        data_sbin = data_sbin.to_dict()
        return data_sbin




    ####---------------------history----------
    ### update sku sumary

    @app.callback(
        Output('dff_history','data'),
        Output('colors_history','value'),
        Input('data_history_app2', 'data'),
        Input('dropdown_top_sbin', 'value'),
    )
    def update_data(data_history,dropdown_history):
        df = pd.DataFrame(data_history)
        lst_fail = get_top_sbin_history(df,dropdown_history)
        df,top_5sbin_fail = map_SBin_history(df,lst_fail)
        colors = map_color_sbin(top_5sbin_fail)
        df = df.to_dict()
        return df,colors

    @app.callback(
        Output('sku_fig_sumary_history','figure'),
        Input('dff_history','data'), 
        Input('colors_history','value'),
    )

    def update_sku_by_time_history(dff,colors):
        
        dff = pd.DataFrame(dff) 
        print('lennnnnnnnnnnnnn',len(dff))
        fig = count_by_col(dff,'SKU','lbl_SBin',colors,list(dff.SKU.unique()))  
        title='Count pass/fail by time for SKU'
        fig.update_layout(title= title)
        return fig

    ### update sku
    @app.callback(
        Output('sku_fig_history','figure'),
        Input('dff_history','data'), 
        Input('colors_history','value'),
        Input('dropdown_sku_history','value'),  
    )

    def update_sku_history(dff,colors,dropdown_sku):
        dff = pd.DataFrame(dff)   
        df_sku = dff[dff['SKU']==dropdown_sku]
        name_='SKU '+dropdown_sku
        fig = pie_sumary(df_sku,'lbl_SBin',name_,colors)
        return fig


    ##### rescreeen pie
    @app.callback(
        Output('pie_rescreen_fig','figure'),
        Input('dropdown_lot','value'), 
        Input('dropdown_top_sbin','value'), 
        Input('data_rescreen_by_lot','data'),
    )
    def update_pie_rescreen(dropdown_lot,top_sbin,data_rescreen_by_lot):
        data_rescreen_by_lot = pd.DataFrame(data_rescreen_by_lot)
        # print('=======len data_rescreen_by_lot: =====',len(data_rescreen_by_lot))
        if(top_sbin==0):
            chart = pie_lot_rescreen_all_sbin(data_rescreen_by_lot,dropdown_lot)
        else:
            chart = pie_lot_rescreen(data_rescreen_by_lot,dropdown_lot,top_sbin)
        return chart

    ##### rescreeen bar
    @app.callback(
        Output('bar_rescreen_fig','figure'),
        Input('dropdown_lot','value'),  
        Input('data_rescreen_by_lot','data'),
    )
    def update_pie_rescreen(dropdown_lot,data_rescreen_by_lot):
        data_rescreen_by_lot = pd.DataFrame(data_rescreen_by_lot)
        # print('=======len data_rescreen_by_lot: =====',len(data_rescreen_by_lot))
        chart = bar_chart_rescreen_lot(data_rescreen_by_lot,dropdown_lot)
        return chart
