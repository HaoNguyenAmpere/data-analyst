# Import required libraries
import plotly
import pandas as pd
from config import Config
import sys
sys.path.append(Config.path_project)
import analyzer
from analyzer.slt_plot_report import *
from analyzer.slt_plot_rescreen import *
# from app.utils.slt_plot_report import *
import json
import datetime as dt
import random
import dash                              # pip install dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input
# from app.utils.slt_plot_rescreen import *
from dash_extensions import Lottie       # pip install dash-extensions
import dash_bootstrap_components as dbc
import plotly.io as pio
import sqlite3
from pandas import DataFrame
import os

from datetime import date
pd.options.mode.chained_assignment = None  # default='warn'
from app.data.data import Data
from app.dash_shared import shared_dash_nav_links
import time 
s =time.time()

df_tmp = Data.get_data_report()
df_tmp_history = Data.get_data_history()
df_set_up = Data.get_data_setup_lot()

df_tmp_history=df_tmp_history[df_tmp_history.TestStep.isin(['FS','RS1','RS2'])]
df_set_up['WaferLotID']= df_set_up.apply(lambda x: split_lot(x['Number']), axis=1)
df_set_up = df_set_up[df_set_up['WaferLotID'].isin(lst_correclation) == False]

sku_unique = ['64.30','80.30']
lst_pass=[1,120,130]
label= 'label_SBin'
label_isChar=True
handler_unique = df_tmp.HandlerID.unique()
lot_unique = [value for value in list(df_set_up.Number.unique()) if value in list(df_tmp_history.LotNumber.unique())] #df_set_up.Number.unique()
print(time.time()-s)

def plotly_sample_app():
# # Create global chart template
    mapbox_access_token = "pk.eyJ1IjoicGxvdGx5bWFwYm94IiwiYSI6ImNrOWJqb2F4djBnMjEzbG50amg0dnJieG4ifQ.Zme1-Uzoi75IaFbieBDl3A"

    layout = dict(
        autosize=True,
        automargin=True,
        margin=dict(l=30, r=30, b=20, t=40),
        hovermode="closest",
        plot_bgcolor="#F9F9F9",
        paper_bgcolor="#F9F9F9",
        legend=dict(font=dict(size=10), orientation="h"),
        title="Satellite Overview",
        mapbox=dict(
            accesstoken=mapbox_access_token,
            style="dark",
            center=dict(lon=-78.05, lat=42.54),
            zoom=7,
        ),
    )

    # Create app layout
    layout2 = html.Div(
        [
            dcc.Store(id='data_setup_lot_app2'),
            dcc.Store(id='lst_set_up_lot_app2'),
            dcc.Store(id='data_history_app2'),
            dcc.Store(id='dff_history'),
            dcc.Store(id='colors_history'),
            dcc.Store(id='data_rescreen'),
            dcc.Store(id='data_rescreen_sbin'),
            dcc.Store(id='data_rescreen_by_lot'),

            html.Div(
                [
                    # html.Div(
                    #     [
                    #         html.Img(
                    #             src=app.get_asset_url("./ampere.png"),
                    #             id="ampere",
                    #             style={
                    #                 "height": "60px",
                    #                 "width": "auto",
                    #                 "margin-bottom": "25px",
                    #             },
                    #         )
                    #     ],
                    #     className="one-third column",
                    # ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H3(
                                        "Data Analyst",
                                        style={"margin-bottom": "0px"},
                                    ),
                                    html.H5(
                                        "Production Overview", style={"margin-top": "0px"}
                                    ),
                                ]
                            )
                        ],
                        className="one-half column",
                        id="title",
                    ),
                    html.Div(
                        [
                            html.A(
                                html.Button("About us", id="learn-more-button"),
                                href="https://amperecomputing.com/",
                            )
                        ],
                        className="one-third column",
                        id="button",
                    ),
                ],
                id="header",
                className="row flex-display",
                style={"margin-bottom": "25px"},
            ),
            html.Div(
                [   
                    html.Div([
                        html.Div(
                                        [html.P("New Latest Update Time"),html.P(id="name_max_day")],
                                        
                                        className="mini_container",
                                    ),
                    
                        # html.Div([
                        #     html.P('New Latest Update from '),
                        #     html.Div(id = 'name_max_day',className="control_label"),]),
                        
                        dcc.Interval(
                            id='interval-component',
                            interval=1*300000, # in milliseconds
                            n_intervals=0
                            ),
                        html.P("Select number sbin to show"), 
                        dcc.Dropdown(
                            id='dropdown_top_sbin',
                            options=[
                                {'label': 'top 1', 'value': 1},
                                {'label': 'top 2', 'value': 2},
                                {'label': 'top 3', 'value': 3},
                                {'label': 'top 4', 'value': 4},
                                {'label': 'top 5', 'value': 5},
                                {'label': 'all sbin', 'value': 0}
                            ],
                            value=0,
                            # placeholder="Select handler",
                            style=dict(
                                # width='60%',
                                verticalAlign="middle"
                                ),
                            
                            ),
                        dcc.Dropdown(
                        id='dropdown_lot',
                        options=[{'label':name, 'value':name} for name in lot_unique],
                        value=lot_unique[0],
                        placeholder="Select LotNumber",
                        style=dict(
                            # width='60%',
                            verticalAlign="middle"
                            )
                        ),
                    
                        
                        ],className="pretty_container_left"),
                    
                    
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Div(
                                        [html.H6(id="n_part"), html.P("Number Parts")],
                                        id="wells",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="n_pass"), html.P("Pass")],
                                        id="gas",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="n_fail"), html.P("Fail")],
                                        id="oil",
                                        className="mini_container",
                                    ),
                            
                                ],
                                id="info-container",
                                className="row container-display",
                            ),
                            html.Div(
                                [
                                    dcc.Loading(id="loading-2",
                                        children=[html.Div(dcc.Graph(id='bar_rescreen_fig'))], type="circle",
                                    ),
                                    # dcc.Graph(id="bar_rescreen_fig")
                                    ],
                                id="countGraphContainer",
                                className="pretty_container",
                            ),
                        ],
                        id="right-column",
                        className="eight columns",
                    ),
                ],
                className="row flex-display",
            ),
            html.Div([
                        dcc.Loading(id="loading-3",
                                        children=[html.Div(dcc.Graph(id='pie_rescreen_fig'))], type="circle",
                                    ),
                        # dcc.Graph(id='pie_rescreen_fig', figure={}, config={'displayModeBar': True}),
                        ],className="mini_container" ,id="cross-filter-options",),
            html.Div(
                [
                    html.Div(
                        [
                            # dcc.Graph(id="sku_fig_sumary_history")
                            dcc.Loading(id="loading-4",
                                        children=[html.Div(dcc.Graph(id='sku_fig_sumary_history'))], type="circle",
                                    ),
                        ],
                        className="pretty_container seven columns",
                    ),
                    html.Div([
                        dcc.Dropdown(
                        id='dropdown_sku_history',
                        options=[{'label':name, 'value':name} for name in ['80.30','64.30']],
                        value='80.30',
                        placeholder="Select Top ",
                        style=dict(
                            width='60%',
                            verticalAlign="middle"
                            )
                        )   ,  
                        # dcc.Graph(id='sku_fig_history', figure={}, config={'displayModeBar': True}),
                        dcc.Loading(id="loading-5",
                                        children=[html.Div(dcc.Graph(id='sku_fig_history'))], type="circle",
                                    ),
                        
                    ],className="pretty_container five columns",),
                ],
                className="row flex-display",
            ),
        
        # dcc.Store(id='data_setup_lot_app2'),
        # dcc.Store(id='lst_set_up_lot_app2'),
        # dcc.Store(id='data_history_app2'),
        # dcc.Store(id='dff_history'),
        # dcc.Store(id='colors_history'),
        # dcc.Store(id='data_rescreen'),
        # dcc.Store(id='data_rescreen_sbin'),
        # dcc.Store(id='data_rescreen_by_lot'),
        ],
        id="mainContainer",
        style={"display": "flex", "flex-direction": "column"},
    )
    return layout2

layout = html.Div(
    id='app_2_page_layout',
    children=[
        # shared_dash_nav_links(),
        plotly_sample_app(),
        # other layout functions can go here.
    ]
)









    # if __name__=='__main__':
    #     app.run_server(debug=False, port=8055)
