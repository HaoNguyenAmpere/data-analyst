import http
import json
import os
import requests
import textwrap
import pandas as pd
import sqlite3
from app.utils.slt_plot_report import *
from app.utils.slt_plot_rescreen import *

def get():
  
    api_uri = 'http://10.76.233.192/search/amkor/api/v1/lot' 
    resp_data = requests.get(api_uri)
    print(resp_data)
    search_resp_data = json.loads(resp_data.text)
    print(len(search_resp_data))
    if resp_data.status_code == http.HTTPStatus.OK:
        if isinstance(search_resp_data, dict):
            return search_resp_data.get('Results', [])
        elif isinstance(search_resp_data, list):
            return search_resp_data
        else:
            return []
    else:
        print(resp_data.status_code, resp_data.text)
        return []

conn = sqlite3.connect('DB_SLT.db')
c = conn.cursor()
listOfTables = c.execute("""SELECT * FROM sqlite_master WHERE type='table' AND name='SETUP_LOT'; """).fetchall()

if listOfTables == []:
    print('Table not found! => create new DB')
    ret_data = get()   
    df= pd.DataFrame(ret_data)
    df = handle_setup_lot(df)
    c.execute('CREATE TABLE SETUP_LOT (AVS, BinLabel, BoardSerial, Cores, CpuType,ECID, Environments, Frequency, HBin, HandlerID,LastModified, LogFile, LotNumber, Operator, PartId,ReportFile, Result, RunTime, SBin, ScreeningMode, Serial,SocketSerial, StartTime, SummaryFile, TDP, TestCase,TestStep, TesterNumber, id,Day,parse_CreatedOn,WaferLotID)')
    conn.commit()
    df.to_sql('SETUP_LOT', conn, if_exists='replace', index = False)
    c.execute('''  
    SELECT * FROM SETUP_LOT''')

else:
    print('Table found! => Insert into DB')
    ret_data = get()   
    df= pd.DataFrame(ret_data)
    print('len',len(df))

    conn = sqlite3.connect('DB_SLT.db')
    df.to_sql('SETUP_LOT', conn, if_exists='replace', index = False)
    print('okie')


       