## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is simple webpage data analyst
	
## Technologies
Project is created with:
* Plotly
* Dash
* Flask
	
## Setup
To run this web

```
$ cd webapp
$ bash ./bash.sh
$ python app.py
```
To export weekly report HTML
```
$ cd webapp
$ python slt_weekly_report.py -s '07/26/2021' -d '08/01/2021' 
```