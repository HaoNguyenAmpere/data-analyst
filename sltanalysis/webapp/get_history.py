

import argparse
import datetime
import sqlite3
from config import Config
import sys
sys.path.append(Config.path_project)
import analyzer
from analyzer.slt_plot_report import *
from analyzer.slt_query_history import *

if __name__ == '__main__':
    # arguments parsing
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('-H', '--host', default=DEFAULT_HOST,
                             help='Specify Server IP address, default is "{}"'.format(DEFAULT_HOST))
    args_parser.add_argument('-P', '--port', default=DEFAULT_PORT, type=int,
                             help='Specify Server Port number, default is "{}"'.format(DEFAULT_PORT))
    args_parser.add_argument('-p', '--part', nargs='+', required=False,
                             help='List of Part ID / ECID / Serial')
    args_parser.add_argument('-l', '--lot', nargs='+', required=False,
                             help='List of Lot Numbers')
    args_parser.add_argument('-b', '--bench', nargs='+', required=False,
                             help='List of Bench ID')
    args_parser.add_argument('-t', '--test', nargs='+', required=False,
                             help='List of Test Cases')
    args_parser.add_argument('-r', '--result', nargs='+', required=False,
                             help='List of Test Results or BIN')
    args_parser.add_argument('-e', '--environment', nargs='+', required=False,
                             help='''Test Environments:
                             + CPU Clock (Mhz)
                             + SoC Voltage (mV)
                             + SoC Clock (Mhz)
                             + PCP Voltage (mV)
                             + PCP Clock (Mhz)
                             + AXI Clock (Mhz)
                             + DDR1 Voltage (mV)
                             + DDR2 Voltage (mV)
                             + DDR Info
                             + ATF Firmware
                             + BIOS Firmware
                             + SCP Firmware
                             + RCA Voltage (mV)
                             + VPP Voltage (mV)
                             ''')
    args_parser.add_argument('-m', '--screen', type=str, required=False,
                             help='Screening mode: Screening/Calibraion/FA/Debug')
    args_parser.add_argument('-s', '--start', type=str, required=False,
                             help='Start date. Format MM/DD/YYYY')
    args_parser.add_argument('-d', '--end', type=str, required=False,
                             help='End date. Format MM/DD/YYYY')

    args = args_parser.parse_args()
    server_host = args.host
    server_port = args.port
    parts = args.part
    lots = args.lot
    benches = args.bench
    tests = args.test
    results = args.result
    environments = args.environment
    screen_mode = args.screen
    start_date = args.start
    end_date = args.end
    # start_date = '05/20/2021'
    # end_date = '07/24/2021'

    conn = sqlite3.connect('DB_SLT.db')
    c = conn.cursor()
    listOfTables = c.execute("""SELECT * FROM sqlite_master WHERE type='table' AND name='SLT_HISTORY'; """).fetchall()

    ### if not exist this table 
    if listOfTables == []:
        print('Table not found! => create new DB')
        ret_data = get_history(server_host, server_port, partNumbers=parts, lotNumbers=lots, benchNumbers=benches, testCases=tests,
                testResults=results, testEnvironments=environments, screeningMode=screen_mode, startDate=start_date,
                endDate=end_date)
        df= pd.DataFrame(ret_data)
        df['StartTime'] = pd.to_datetime(df['StartTime'])
        df.StartTime =df.StartTime.dt.tz_convert('Asia/Seoul')
        c.execute('CREATE TABLE SLT_HISTORY (AVS, BinLabel, BoardSerial, Cores, CpuType,ECID, Environments, Frequency, HBin, HandlerID,LastModified, LogFile, LotNumber, Operator, PartId,ReportFile, Result, RunTime, SBin, ScreeningMode, Serial,SocketSerial, StartTime, SummaryFile, TDP, TestCase,TestStep, TesterNumber, id)')
        conn.commit()
        df.to_sql('SLT_HISTORY', conn, if_exists='replace', index = False)
        c.execute('''  
        SELECT * FROM SLT_HISTORY''')

    else:
        print('Table found! => Insert into DB')
        max_date = c.execute("""SELECT max(Day) FROM SLT_HISTORY""").fetchall()
        cr_date = max_date[0][0]
        cr_date = datetime.datetime.strptime(cr_date, '%Y-%m-%d')
        cr_date = cr_date.strftime("%m/%d/%Y")
        start_date = cr_date
        ret_data = get_history(server_host, server_port, partNumbers=parts, lotNumbers=lots, benchNumbers=benches, testCases=tests,
                testResults=results, testEnvironments=environments, screeningMode=screen_mode, startDate=start_date,endDate=end_date)
        df= pd.DataFrame(ret_data)
        df['StartTime'] = pd.to_datetime(df['StartTime'])
        df.StartTime =df.StartTime.dt.tz_convert('Asia/Seoul')
        conn = sqlite3.connect('DB_SLT.db')
        df.to_sql('SLT_HISTORY', conn, if_exists='append', index = False)
        print('okie')


    

    # ret_data = get_history(server_host, server_port, partNumbers=parts, lotNumbers=lots, benchNumbers=benches, testCases=tests,
    #             testResults=results, testEnvironments=environments, screeningMode=screen_mode, startDate=start_date,
    #             endDate=end_date)
    # df= pd.DataFrame(ret_data)
    # print(len(df))
    # df['StartTime'] = pd.to_datetime(df['StartTime'])
    # df.StartTime =df.StartTime.dt.tz_convert('Asia/Seoul')
 
    # df.to_sql('SLT_HISTORY', conn, if_exists='replace', index = False)
