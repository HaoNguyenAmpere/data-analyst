
from app.utils.slt_plot_report import *

from app.utils.slt_plot_rescreen import *
import time
path_db = 'DB_SLT.db'
s= time.time()
conn = sqlite3.connect(path_db)

### data SETUP_LOT
df_setup_lot = pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
df_setup_lot = handle_setup_lot(df_setup_lot)
lst_set_up_lot = list(df_setup_lot['Number'].unique())

## Data SLT_REPORT
df_report = pd.read_sql('''SELECT * FROM SLT_REPORT''', conn)
df_report = handle_data(df_report)
df_report = handle_data_report(df_report,lst_set_up_lot)

### data SLT_HISTORY
df_history = pd.read_sql('''SELECT * FROM SLT_HISTORY''', conn)
df_history = handle_data(df_history)
df_history = handle_data_history(df_history,lst_set_up_lot)

### data SLT_RESCREEN
data_count = count_part_setup_lot(df_history,df_setup_lot,'count_part_setup_lot_event_.xlsx')
data_count_clean = clean_data_count_event(data_count)
data_rescreen = sumary_rescreen(df_history,data_count_clean,'Lot_sumary.xlsx')
data_rescreen.columns = ['_'.join(col) for col in data_rescreen.columns]


### save to Database
df_report.to_sql('SLT_REPORT', conn, if_exists='replace', index = False)
df_history.to_sql('SLT_HISTORY', conn, if_exists='replace', index = False)
df_setup_lot.to_sql('SETUP_LOT', conn, if_exists='replace', index = False)


conn = sqlite3.connect('DB_SLT.db')
c = conn.cursor()
listOfTables = c.execute("""SELECT * FROM sqlite_master WHERE type='table' AND name='SLT_RESCREEN'; """).fetchall()

### if not exist this table 
if listOfTables == []:
    c.execute('CREATE TABLE SLT_RESCREEN (FS_Lot, FS_SBIN, FS_Result,FS_Test_program,FS_Yield,RS1_Result,RS1_Test_program,RS1_Pass,RS1_Fail_Same_SBin,RS1_Fail_Other_SBIN,RS1_Sbin recovery,RS1_Lot_recovery,RS2_Result,RS2_Test_program,RS2_Pass,RS2_Fail_Same_SBin,RS2_Fail_Other_SBIN,RS2_Sbin recovery,RS2_Lot_recovery)')
    conn.commit()

data_rescreen.to_sql('SLT_RESCREEN', conn, if_exists='replace', index = False)


