import argparse
import pandas as pd
import plotly
import sys
import json
import datetime as dt
import random
import plotly.io as pio
import sqlite3
from pandas import DataFrame
from config import Config
import os,sys
sys.path.append(Config.path_project)
import analyzer
from analyzer.slt_plot_report import *
# label='SBin'
n_top_sbin=5 
lst_correclation = ['TAAF54','TA7H13','TA3T23','TAAF54','TAAF51','TAAF57','TAAM50','TARW31']
# start_date = '07/26/2021'
# end_date = '08/01/2021'

if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('-s', '--start', type=str, required=False,
                             help='Start date. Format MM/DD/YYYY')
    args_parser.add_argument('-d', '--end', type=str, required=False,
                             help='End date. Format MM/DD/YYYY')

    args = args_parser.parse_args()
    start_date = args.start
    end_date = args.end
  
    ###### ------------------------------------------ GET DATA---------------------------------------------------

    conn = sqlite3.connect('DB_SLT.db')
    df=pd.read_sql('''SELECT * FROM SLT_REPORT''', conn)
    print('len****',len(df))
    df_setup_lot=pd.read_sql('''SELECT * FROM SETUP_LOT''', conn)
    #### -------------------------------------------- HANDLE DATA --------------------------------------------------
    df_setup_lot = handle_setup_lot(df_setup_lot)
    lst_set_up_lot = list(df_setup_lot['Number'].unique())

    df = handle_data(df)
    df['Month_Day_Year'] = pd.to_datetime(df['StartTime']).apply(lambda x: '{month}/{day}/{year}'.format( month='{:02d}'.format(x.month),day='{:02d}'.format(x.day),year=x.year))
    df['Month_Day_Year'] =df['Month_Day_Year'].astype(str)
    if(start_date!=None and end_date!=None):
        df = df[(df['Month_Day_Year'] >= start_date) & (df['Month_Day_Year'] <= end_date)]
    max_day=df['Day'].values.max()
    min_day=df['Day'].values.min()
    name_day = 'from '+min_day+' to '+max_day
    df['WaferLotID']= df.apply(lambda x: split_lot(x['LotNumber']), axis=1)
    df = df[df['WaferLotID'].isin(lst_correclation) == False]
    df = df[df.LotNumber.isin(lst_set_up_lot)]
    print('lennnnnn dfffff',len(df))
    data=[]
    for lot in df.LotNumber.unique():
        df_lot = df[df.LotNumber==lot]
        if(len(lot.split('.'))>2):
            df_lot['SKU']= df_lot.apply(lambda x: split_sku(x['LotNumber']), axis=1)
            data.append(df_lot)
        else:
            df_lot['SKU']= df_lot['Cores']+'.'+df_lot['Frequency']
            df_lot['SKU']= df_lot.apply(lambda x: create_sku(x['SKU']), axis=1)
            data.append(df_lot)

    data =pd.concat(data)
    data = data[~data.SKU.isnull()]
    df = data[data.SKU !='None.None']
    print('========len========',len(df))

    ### Show Fail by SBin => to find top sbin
    lst_top_sbin_fail = get_top_fail_sbin(df,n_top_sbin) 
    
    df,top_5sbin_fail = map_SBin(df,lst_top_sbin_fail)


    label= 'label_SBin'
    label_isChar=True
    name_report='REPORT BY SBIN ' #+type_data
    colors = map_color_sbin(top_5sbin_fail)
    color_discrete_map_line = colors


    if(len(df['Week'].unique())<15):
        show_by_week = True
        group_col = 'Week'
    else:
        show_by_week = False
        group_col = 'YearMonth'

    lst =[]
    for (i,r) in df.iterrows():
        e = r['Environments']
        e=json.loads(e)
        e['Label'] = r['Label']
        e['label_SBin'] = r['label_SBin']
        e['YearMonthDay'] = r['YearMonthDay']
        e['id'] = r['id']
        lst.append(e)
    env_df = pd.DataFrame.from_dict(lst) 

    env_df['PCP Voltage']=env_df['PCP Voltage'].str.replace(' mV', '')
    env_df['Leak Value']=env_df['Leak Value'].str.replace(' mV', '')

    env_df['Leak Value']=env_df['Leak Value'].replace('None',np.nan)
    env_df['Leak Value']=env_df['Leak Value'].replace('N/A',np.nan)
    env_df['PCP Voltage']=env_df['PCP Voltage'].replace('None',np.nan)
    env_df['PCP Voltage']= env_df['PCP Voltage'].replace('N/A',np.nan)

    env_df['Leak Value'].astype(float)
    env_df['PCP Voltage'].astype(float)
    env_df['Leak Value']=pd.to_numeric(env_df['Leak Value'])  
    env_df['PCP Voltage']=pd.to_numeric(env_df['PCP Voltage'])

    df.Cores = df.Cores.replace('64', '0x64')
    df.Cores = df.Cores.replace('80', '0x80')
    df.Frequency = df.Frequency.replace('30', '0x30')
    df.Frequency = df.Frequency.replace('33', '0x33')
    df['time'] =  pd.to_datetime(df['StartTime'])
    print('========len========',len(df))

    #-----------------------------------------------DRAW CHART -----------------------------------------

    fig_count_fail_sbin = count_fail_sbin(df) 
    fig_count_fail_sbin_html = convert_fig_to_html(fig_count_fail_sbin)
    ### Sumary pass/fail
    fig_pie_sumarry_html = convert_fig_to_html(pie_sumary(df,label,'Sumary',colors))
    # print(fig_pie_sumarry_html)
    ####
    fig_Handler_html = convert_fig_to_html(count_by_col(df,'HandlerID',label,colors,list(df.HandlerID.unique())))

    ####
    fig_Handler_monthly_sumary_html = convert_fig_to_html(sumary_by_time(df,group_col,'HandlerID',label,colors,list(df.HandlerID.unique())))

    ####
    value_HandlerID = df['HandlerID'].value_counts().sort_index(ascending=True).index
    fig_HandlerID_monthly_html = show_monthly_col(df,'HandlerID',value_HandlerID,group_col,label,colors,label_isChar,color_discrete_map_line)

    ####
    fig_sumary_site_by_hontech_html = convert_fig_to_html(sumary_site_by_hontech(df,'TesterNumber','HandlerID',colors_handler,True))

  

    ######
    fig_sku_monthly_sumary_html = convert_fig_to_html(sumary_by_time(df,group_col,'SKU',label,colors,list(df.SKU.unique())))

    fig_LeakValue_html = convert_fig_to_html(count_by_col(env_df,'Leak Value',label,colors,label_isChar))
    fig_PCPVoltage_html = convert_fig_to_html(count_by_col(env_df,'PCP Voltage',label,colors,label_isChar))


    ### from top sbin: show sbin fail  by testernumber and handlerID
    lst_fig_top_fail_by_site_html = []
    for sbin in lst_top_sbin_fail:
        df_fail=df[df['Label']=='fail']
        df_fail_sbin=df_fail[df_fail.SBin==sbin]
        fig= top_fail_by_site(df_fail_sbin,'TesterNumber','HandlerID',colors_handler,True,sbin)
        name_='top_fail_by_site'+str(sbin)
        lst_fig_top_fail_by_site_html.append(convert_fig_to_html(fig))

    #####
    fig_sku_html=[]
    for idx,core in enumerate (['80.30','64.30']):
        df_cores=df[df['SKU']==core]
        if(len(df_cores)>0):
            name_='SKU '+core
            fig = pie_sumary(df_cores,label,name_,colors)
            fig_sku_html.append(convert_fig_to_html(fig))

    ####
    fig_TesterNumber_html=[]
    for handler in df['HandlerID'].value_counts().sort_index(ascending=True).index:
        df_handler=df[df['HandlerID']==handler]
        fig = count_by_col(df_handler,'TesterNumber',label,colors,list(df.TesterNumber.unique()))
        title='Count pass/fail by HandlerID: '+handler
        fig.update_layout(title= title)
        fig_TesterNumber_html.append(convert_fig_to_html(fig))

    ####
    fig_TesterNumber_monthly_sumary_html=[]
    lst_fig=[]
    for handler in df['HandlerID'].value_counts().sort_index(ascending=True).index:
        df_handler=df[df['HandlerID']==handler]
        fig = sumary_by_time(df_handler,group_col,'TesterNumber',label,colors,list(df.TesterNumber.unique()))
        title='Count pass/fail by HandlerID: '+handler
        fig.update_layout(title= title)
        fig_TesterNumber_monthly_sumary_html.append(convert_fig_to_html(fig))

    #-----------------------------------------------Write Report HTML -----------------------------------------

    interactive_report = ''
    static_report = ''

    interactive_report=interactive_report+'<h1>'+name_report+'</h1>'
    interactive_report=interactive_report+'<h3>'+name_day+'</h3>'


    interactive_report=interactive_report+'<h3>Show Fail by SBin</h3>'
    interactive_report += fig_count_fail_sbin_html
    interactive_report = interactive_report+'<h3>show fail by site in top SBin Fail</h3>'
    interactive_report = add_lst_html(interactive_report,lst_fig_top_fail_by_site_html)
    interactive_report = interactive_report+'<h3>Sumarry pass/fail of our data</h3>' 
    interactive_report = interactive_report+ fig_pie_sumarry_html

    interactive_report=interactive_report+'<h2>1. HandlerID</h2>'
    interactive_report=interactive_report+'<h3>1.1 Show pass/fail by HandlerID</h3>'
    interactive_report += fig_Handler_html
    interactive_report=interactive_report+'<h3>1.2 Show pass/fail for each HandlerID by time</h3>'
    interactive_report += fig_Handler_monthly_sumary_html
    interactive_report = add_lst_html(interactive_report,fig_HandlerID_monthly_html)

    interactive_report=interactive_report+'<h2> 2.TesterNumber</h2>'
    interactive_report += fig_sumary_site_by_hontech_html
    interactive_report=interactive_report+'<h3>2.1 Show pass/fail TesterNumber for each handlerID</h3>'
    interactive_report = add_lst_html(interactive_report,fig_TesterNumber_html)
    interactive_report=interactive_report+'<h3>2.2 Show pass/fail for each TesterNumber for each handlerID by time</h3>'
    interactive_report =add_lst_html(interactive_report,fig_TesterNumber_monthly_sumary_html)

    interactive_report=interactive_report+'<h2>4. SKU </h2>'
    nteractive_report=interactive_report+'<h3>4.1 Show pass/fail SKU</h3>'
    interactive_report = add_lst_html(interactive_report,fig_sku_html)
    interactive_report=interactive_report+'<h3>4.2 Show pass/fail SKU by time</h3>'
    interactive_report += fig_sku_monthly_sumary_html


    interactive_report=interactive_report+'<h2>6. PCP Voltage</h2>'
    interactive_report=interactive_report+'<h3>6.1 Show pass/fail by PCP Voltage</h3>'
    interactive_report +=fig_PCPVoltage_html

    interactive_report=interactive_report+'<h2>6. Leak Value</h2>'
    interactive_report=interactive_report+'<h3>6.1 Show pass/fail by Leak Value</h3>'
    interactive_report +=fig_LeakValue_html


    from datetime import date
    today = date.today()
    today = today.strftime("%m_%d_%y")
    name_file_save = 'report day_'+str(today)+'.html'
    print(name_file_save)
    with open(name_file_save, 'w') as f:
        f.write(interactive_report)

